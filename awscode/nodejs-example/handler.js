'use strict';

function delay(time) {
  return new Promise(resolve => setTimeout(resolve, time));
} 

function setENV() {
  process.env['USER_ID'] = '123';
  process.env['NAME'] = 'duc';
}

module.exports.hello =  (event) => {
  // setENV()
   console.log("userID:",process.env.USER_ID)
  console.log("name:",process.env.NAME)

  var respData = {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: 'function hello',
        input: event,
      },
      null,
      2
    ),
  };
  //  delay(4000);
  return respData;

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};

// console.log(exports.hello("log"))