'use strict';

const AWS = require("aws-sdk");
const ses = new AWS.SES();

module.exports.createContact = async (event) => {

    console.log("event:", event);

    const { to, from, subject, message } = JSON.parse(event.body);

    console.log(to, from, subject, message);

    if (!to || !from || !subject || !message) {
        return {
            statusCode: 400,
            body: JSON.stringify(
                {
                    message: 'to, from subject, message invalid',
                    input: event,
                },
                null,
                2
            ),
        };
    }

    const params = {
        Destination: {
            ToAddresses: [to]
        },
        Message: {
            Body: {
                Text: {
                    Data: message
                }
            },
            Subject: {
                Data: subject
            }
        },
        Source: from
    }

    try {
        await ses.sendEmail(params).promise();
        return {
            statusCode: 200,
            body: JSON.stringify(
                {
                    message: 'send ok',
                    success: true
                }
            )
        };
    } catch (err) {
        console.error(err);
        return {
            statusCode: 2400,
            body: JSON.stringify(
                {
                    message: 'send err',
                    success: false
                }
            )
        };
    }
    // end
}


