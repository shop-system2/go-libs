package main

func upload(ctx context.Context, req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	return events.APIGatewayProxyResponse{}, error
}

func main() {
	lambda.Start(upload)
}
