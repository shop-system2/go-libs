package awscode

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

var TableName = "metadata_user"

//
func CreateDynamoClient(region string) *dynamodb.DynamoDB {
	if region == "" {
		region = "us-west-2"
	}
	config := aws.NewConfig()
	config.Region = aws.String(region)
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
		Config:            *config,
	}))
	return dynamodb.New(sess)
}

//
func GetListTable(svc *dynamodb.DynamoDB) {
	fmt.Println("GetListTable")
	input := &dynamodb.ListTablesInput{}
	fmt.Printf("Tables:\n")
	for {
		// Get the list of tables
		result, err := svc.ListTables(input)
		if err != nil {
			if aerr, ok := err.(awserr.Error); ok {
				switch aerr.Code() {
				case dynamodb.ErrCodeInternalServerError:
					fmt.Println(dynamodb.ErrCodeInternalServerError, aerr.Error())
				default:
					fmt.Println(aerr.Error())
				}
			} else {
				// Print the error, cast err to awserr.Error to get the Code and
				// Message from an error.
				fmt.Println(err.Error())
			}
			return
		}

		for i, n := range result.TableNames {
			fmt.Println("tableName:", i, *n)
		}

		// assign the last read tablename as the start for our next call to the ListTables function
		// the maximum number of table names returned in a call is 100 (default), which requires us to make
		// multiple calls to the ListTables function to retrieve all table names
		input.ExclusiveStartTableName = result.LastEvaluatedTableName

		if result.LastEvaluatedTableName == nil {
			break
		}
	}
}

// add item in dynamoDB
type Item struct {
	Cccd string `json:"cccd"`
}

func getItems() []Item {
	raw, err := ioutil.ReadFile("./movie_data.json")
	if err != nil {
		log.Fatalf("Got error reading file: %s", err)
	}

	var items []Item
	json.Unmarshal(raw, &items)
	return items
}

func AddItems(svc *dynamodb.DynamoDB) {
	fmt.Println("AddItems")
	items := getItems()

	for _, item := range items {
		av, err := dynamodbattribute.MarshalMap(item)
		if err != nil {
			log.Fatalf("Got error marshalling map: %s", err)
		}

		input := &dynamodb.PutItemInput{
			Item:      av,
			TableName: aws.String(TableName),
		}

		_, err = svc.PutItem(input)
		if err != nil {
			log.Fatalf("Got error calling PutItem: %s", err)
		}

		fmt.Println("Successfully added '" + item.Cccd + "' to table " + TableName)
	}
}

// get single
func GetDataItem(svc *dynamodb.DynamoDB) {

	result, err := svc.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String(TableName),
		Key: map[string]*dynamodb.AttributeValue{
			"cccd": {
				S: aws.String("225733111"),
			},
		},
	})
	if err != nil {
		log.Fatalf("Got error calling GetItem: %s", err)
	}
	if result.Item == nil {
		panic("result not found")
	}

	item := Item{}

	err = dynamodbattribute.UnmarshalMap(result.Item, &item)
	if err != nil {
		panic(fmt.Sprintf("Failed to unmarshal Record, %v", err))
	}
	fmt.Println("item:", item)
}

// get all item
func GetAllItem(svc *dynamodb.DynamoDB) {
	filt := expression.Name("cccd").BeginsWith("225733")
	proj := expression.NamesList(expression.Name("cccd"))
	expr, err := expression.NewBuilder().WithFilter(filt).WithProjection(proj).Build()
	if err != nil {
		log.Fatalf("Got error building expression: %s", err)
	}

	params := &dynamodb.ScanInput{
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		FilterExpression:          expr.Filter(),
		ProjectionExpression:      expr.Projection(),
		TableName:                 aws.String(TableName),
	}

	// Make the DynamoDB Query API call
	result, err := svc.Scan(params)
	if err != nil {
		log.Fatalf("Query API call failed: %s", err)
	}
	for _, i := range result.Items {
		item := Item{}

		err = dynamodbattribute.UnmarshalMap(i, &item)

		if err != nil {
			log.Fatalf("Got error unmarshalling: %s", err)
		}
		fmt.Println(item)

	}
}

func MainAwsCode() {
	fmt.Println("MainAwscode")
	svc := CreateDynamoClient("")
	// GetListTable(svc)
	AddItems(svc)
	// GetDataItem(svc)
	GetAllItem(svc)
}
