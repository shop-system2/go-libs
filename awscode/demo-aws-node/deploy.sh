#!/bin/sh
envStage=$1
functionName=$2

# set env stage default dev
if ([ -z "$envStage" ])
then 
    envStage="dev"
fi

# Safety Checks
if ([ "$envStage" != "dev" ] && [ "$envStage" != "staging" ])
then
    echo "Please provide arguments: Usage: ./deploy.sh [ENV_TO_DEPLOY]"
    echo "Supported environments: dev / staging"
    exit 1
fi
    echo "Deployment started for environment: " $envStage


#Deploying the lambdas via serverless

echo "Starging deploying lambdas"

if ([ -z "$functionName" ])
then 
    echo "deploy all function in stage $envStage"
    serverless deploy --stage $envStage
else
    echo "deploy only function $functionName in stage $envStage"
    sls deploy  --stage $envStage -f $functionName
fi

echo "End deploying successfully"

exit 0