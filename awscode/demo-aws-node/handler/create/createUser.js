const AWS = require("aws-sdk");
const tableName = process.env.USERS_TABLE;
const dynamoDb = new AWS.DynamoDB.DocumentClient();
const uuid = require("uuid");

module.exports.createUser = (event, context, callback) => {

    const timestamp = new Date().getTime();
    const data = JSON.parse(event.body);

    console.log("timestamp:",timestamp)
    console.log("data:",data)

    const params = {
        TableName: tableName,
        Item: {
            userId: uuid.v1(),
            username: data.username,
            age: data.age,
            created_at: timestamp
        },
    };

    dynamoDb.put(params, (error, data) => {
        if (error) {
            console.error(error);
            callback(new Error(error));
            return;
        }
        console.log("exec created ok, item", data.Item )
        // create a response
        const response = {
            statusCode: 200,
            body: JSON.stringify(data.Item),
        };
        callback(null, response);
    });
}