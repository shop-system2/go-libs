const AWS = require("aws-sdk");

const tableName = process.env.USERS_TABLE;
const dynamoDb = new AWS.DynamoDB.DocumentClient();

module.exports.listUser = (event, context, callback) => {
    const params = {
        TableName: tableName,
    };

    dynamoDb.scan(params, (error, data) => {
        if (error) {
            console.error(error);
            callback(new Error(error));
            return;
        }
        const response = {
            statusCode: 200,
            body: JSON.stringify(data.Items),
        };
        callback(null, response);
    });

};
