const AWS = require("aws-sdk");
const tableName = process.env.USERS_TABLE;
const dynamoDb = new AWS.DynamoDB.DocumentClient();

module.exports.deleteUser = (event, context, callback) => {
    const params = {
        TableName: tableName,
        Key: {
            userId: event.pathParameters.id,
        },
    };

    dynamoDb.delete(params, (error, data) => {
        if (error) {
            console.error(error);
            callback(new Error(error));
            return;
        }
    
        const response = {
            statusCode: 200,
            body: JSON.stringify({ data: "deletion user successful!" }),
        };
        callback(null, response);
    });
    
};
