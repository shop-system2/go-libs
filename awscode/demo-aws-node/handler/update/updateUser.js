const AWS = require("aws-sdk");
const tableName = process.env.USERS_TABLE;
const dynamoDb = new AWS.DynamoDB.DocumentClient();

module.exports.updateUser = (event, context, callback) => {

    const datetime = new Date().toISOString();
    const data = JSON.parse(event.body);

    console.log("timestamp:",datetime)
    console.log("data:",data)

    const params = {
        TableName: tableName,
        Key: {
            userId: event.pathParameters.id,
        },
        // ExpressionAttributeNames: {
        //     "#username_text": "username",
        // },
        ExpressionAttributeValues: {
          ":username": data.username,
          ":updated_at": datetime,
          ":age": data.age,
        },
        UpdateExpression:
          "SET username = :username, age = :age, updated_at = :updated_at",
        ReturnValues: "ALL_NEW",
    };

    dynamoDb.update(params, (error, data) => {
        if (error) {
            console.error(error);
            callback(new Error(error));
            return;
        }
        const response = {
            statusCode: 200,
            body: JSON.stringify(data.Attributes),
        };
        callback(null, response);
    });

}
