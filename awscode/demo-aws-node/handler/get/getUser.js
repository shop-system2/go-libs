const AWS = require("aws-sdk");
const tableName = process.env.USERS_TABLE;
const dynamoDb = new AWS.DynamoDB.DocumentClient();

module.exports.getUser = (event, context, callback) => {
    const params = {
      TableName: tableName,
      Key: {
        userId: event.pathParameters.id,
      },
    };
  
    dynamoDb.get(params, (error, data) => {
      if (error) {
        console.error(error);
        callback(new Error(error));
        return;
      }
  
      const response = data.Item
        ? {
            statusCode: 200,
            body: JSON.stringify(data.Item),
          }
        : {
            statusCode: 404,
            body: JSON.stringify({ message: "user not found" }),
          };
      callback(null, response);
    });
  };
  