package main

import (
	"context"
	"fmt"
	awscode "go-libs/awscode"
	awsgo "go-libs/awscode/aws-go"
	"os"

	"github.com/aws/aws-sdk-go/aws"

	"github.com/aws/aws-sdk-go/service/dynamodb"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

func GetTableName() string {
	tableName := os.Getenv("USERS_TABLE")
	if tableName == "" {
		fmt.Println("get env not found")
		tableName = awsgo.TableName
	}
	return tableName
}

func Delete(ctx context.Context, req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	id := req.PathParameters["id"]
	dynamoClient := awscode.CreateDynamoClient("us-east-1")
	if dynamoClient == nil {
		return events.APIGatewayProxyResponse{
			StatusCode: 500,
			Body:       "dynamo client empty",
		}, nil
	}

	_, err := dynamoClient.DeleteItem(&dynamodb.DeleteItemInput{
		TableName: aws.String(GetTableName()),
		Key: map[string]*dynamodb.AttributeValue{
			"userId": {
				S: aws.String(id),
			},
		},
	})
	if err != nil {
		return events.APIGatewayProxyResponse{
			StatusCode: 400,
			Body:       fmt.Sprintf("%s-%v", "delete item err", err),
		}, nil
	}

	return events.APIGatewayProxyResponse{
		StatusCode: 200,
		Body:       "delete user successfully",
	}, nil
}

func main() {
	lambda.Start(Delete)
}
