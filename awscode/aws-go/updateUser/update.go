package main

import (
	"context"
	"encoding/json"
	"fmt"
	awscode "go-libs/awscode"
	awsgo "go-libs/awscode/aws-go"
	"os"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/google/uuid"

	"github.com/aws/aws-sdk-go/service/dynamodb"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

func GetTableName() string {
	tableName := os.Getenv("USERS_TABLE")
	if tableName == "" {
		fmt.Println("get env not found")
		tableName = awsgo.TableName
	}
	return tableName
}

func Create(ctx context.Context, req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {

	fmt.Println("req data:", req.Body)
	reqDTO := awsgo.UserModel{}
	err := json.Unmarshal([]byte(req.Body), &reqDTO)
	if err != nil {
		fmt.Println("unmarshal data failed", err)
		return events.APIGatewayProxyResponse{
			StatusCode: 400,
			Body:       fmt.Sprintf("%s-%v", "unmarshal data failed", err),
		}, nil
	}
	dynamoClient := awscode.CreateDynamoClient("us-east-1")
	if dynamoClient == nil {
		fmt.Println("dynamo client empty", err)
		return events.APIGatewayProxyResponse{
			StatusCode: 500,
			Body:       fmt.Sprintf("%s-%v", "dynamo client empty", err),
		}, nil
	}
	reqDTO.CreatedAt = (time.Now().UnixNano() / int64(time.Millisecond))
	reqDTO.UserID = uuid.NewString()
	fmt.Println("reqDTO:", reqDTO)
	item, err := dynamodbattribute.MarshalMap(reqDTO)
	fmt.Println("itemExec:", item)
	if err != nil {
		fmt.Println("dynamo attribute marshal map failed", err)
		return events.APIGatewayProxyResponse{
			StatusCode: 400,
			Body:       fmt.Sprintf("%s-%v", "dynamo attribute marshal map failed", err),
		}, nil
	}

	itemInput := &dynamodb.PutItemInput{
		Item:      item,
		TableName: aws.String(GetTableName()),
	}
	_, err = dynamoClient.PutItem(itemInput)
	if err != nil {
		fmt.Println("exec put item failed", err)
		return events.APIGatewayProxyResponse{
			StatusCode: 400,
			Body:       fmt.Sprintf("%s-%v", "exec put item failed", err),
		}, nil
	}
	return events.APIGatewayProxyResponse{
		StatusCode: 200,
		Body:       "create user successfully",
	}, nil
}

func main() {
	lambda.Start(Create)
}
