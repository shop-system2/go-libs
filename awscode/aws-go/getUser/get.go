package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"go-libs/awscode"
	awsgo "go-libs/awscode/aws-go"
	"os"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

func GetTableName() string {
	tableName := os.Getenv("USERS_TABLE")
	if tableName == "" {
		fmt.Println("get env not found")
		tableName = awsgo.TableName
	}
	return tableName
}

func Get(ctx context.Context, req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	id := req.PathParameters["id"]
	fmt.Println("paramID:", id)

	dynamoClient := awscode.CreateDynamoClient("us-east-1")
	if dynamoClient == nil {
		return events.APIGatewayProxyResponse{
			StatusCode: 500,
			Body:       "dynamo client empty",
		}, nil
	}

	result, err := dynamoClient.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String(GetTableName()),
		Key: map[string]*dynamodb.AttributeValue{
			"userId": {
				S: aws.String(id),
			},
		},
	})
	if err != nil {
		return events.APIGatewayProxyResponse{
			StatusCode: 400,
			Body:       fmt.Sprintf("%s-%v", "get item err", err),
		}, nil
	}
	if result.Item == nil {
		return events.APIGatewayProxyResponse{
			StatusCode: 400,
			Body:       "item not found",
		}, nil
	}
	user := awsgo.UserModel{}

	err = dynamodbattribute.UnmarshalMap(result.Item, &user)
	if err != nil {
		return events.APIGatewayProxyResponse{
			StatusCode: 400,
			Body:       fmt.Sprintf("%s-%v", "unmarshalmap item err", err),
		}, nil
	}
	var buf bytes.Buffer
	body, err := json.Marshal(map[string]interface{}{
		"message": "ok",
		"data":    user,
	})
	if err != nil {
		return events.APIGatewayProxyResponse{
			StatusCode: 400,
			Body:       fmt.Sprintf("%s-%v", "buf unmarshal response err: ", err),
		}, nil
	}
	json.HTMLEscape(&buf, body)
	return events.APIGatewayProxyResponse{
		StatusCode: 200,
		Body:       buf.String(),
	}, nil
}

func main() {
	lambda.Start(Get)
}
