package awsgo

// constant
const TableName = "go-users-table-dev"

// models
type UserModel struct {
	UserID    string `json:"userId"`
	UserName  string `json:"username"`
	CreatedAt int64  `json:"created_at"`
	UpdatedAt int64  `json:"updated_at"`
	Age       uint   `json:"age"`
}
