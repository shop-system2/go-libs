package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	awscode "go-libs/awscode"
	awsgo "go-libs/awscode/aws-go"
	"os"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
)

func GetTableName() string {
	tableName := os.Getenv("USERS_TABLE")
	if tableName == "" {
		fmt.Println("get env not found")
		tableName = awsgo.TableName
	}
	return tableName
}
func ListUser(ctx context.Context) (events.APIGatewayProxyResponse, error) {
	dynamoClient := awscode.CreateDynamoClient("us-east-1")
	if dynamoClient == nil {
		return events.APIGatewayProxyResponse{
			StatusCode: 400,
			Body:       "dynamo client empty",
		}, nil
	}
	proj := expression.NamesList(expression.Name("userId"), expression.Name("username"), expression.Name("created_at"), expression.Name("age"), expression.Name("updated_at"))
	expr, err := expression.NewBuilder().WithProjection(proj).Build()
	if err != nil {
		return events.APIGatewayProxyResponse{
			StatusCode: 400,
			Body:       fmt.Sprintf("%s-%v", "exec get list data: ", err),
		}, nil
	}

	params := &dynamodb.ScanInput{
		ExpressionAttributeNames:  expr.Names(),
		ExpressionAttributeValues: expr.Values(),
		FilterExpression:          expr.Filter(),
		ProjectionExpression:      expr.Projection(),
		TableName:                 aws.String(GetTableName()),
	}

	// Make the DynamoDB Query API call
	result, err := dynamoClient.Scan(params)
	if err != nil {
		return events.APIGatewayProxyResponse{
			StatusCode: 400,
			Body:       fmt.Sprintf("%s-%v", "dynamo scan err: ", err),
		}, nil
	}
	var listUser = []awsgo.UserModel{}
	for _, i := range result.Items {
		item := awsgo.UserModel{}
		err = dynamodbattribute.UnmarshalMap(i, &item)
		if err != nil {
			return events.APIGatewayProxyResponse{
				StatusCode: 400,
				Body:       fmt.Sprintf("%s-%v", "unmarshal item err: ", err),
			}, nil
		}
		listUser = append(listUser, item)
	}
	var buf bytes.Buffer
	body, err := json.Marshal(map[string]interface{}{
		"message": "ok",
		"data":    listUser,
	})
	if err != nil {
		return events.APIGatewayProxyResponse{
			StatusCode: 400,
			Body:       fmt.Sprintf("%s-%v", "buf unmarshal response err: ", err),
		}, nil
	}
	json.HTMLEscape(&buf, body)
	return events.APIGatewayProxyResponse{
		StatusCode: 200,
		Body:       buf.String(),
	}, nil
}

func main() {
	lambda.Start(ListUser)
}
