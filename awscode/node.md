# aws-lambda-serverless
## section 3
### 3.6
-   demo simple create function run and deploy => ok
-   result:

```
exports.handler = async (event) => {
    console.log("section3.6: testing func lambda")
    console.log("event:", event)
    // TODO implement
    const response = {
        statusCode: 200,
        body: event.key1,
    };
    return response;
};

```
```
A function update is still in progress so the invocation went to the previously deployed code and configuration.

Test Event Name
Testing1

Response
{
  "statusCode": 200,
  "body": "value1"
}

Function Logs
START RequestId: 8ba766eb-01ca-4f6f-8a3d-d05f4751334f Version: $LATEST
2022-07-26T01:50:06.497Z	8ba766eb-01ca-4f6f-8a3d-d05f4751334f	INFO	section3.6: testing func lambda
2022-07-26T01:50:06.499Z	8ba766eb-01ca-4f6f-8a3d-d05f4751334f	INFO	event: { key1: 'value1', key2: 'value2', key3: 'value3' }
END RequestId: 8ba766eb-01ca-4f6f-8a3d-d05f4751334f
REPORT RequestId: 8ba766eb-01ca-4f6f-8a3d-d05f4751334f	Duration: 26.82 ms	Billed Duration: 27 ms	Memory Size: 128 MB	Max Memory Used: 55 MB	Init Duration: 177.02 ms

Request ID
8ba766eb-01ca-4f6f-8a3d-d05f4751334f
```
### 3.7
-   creating, deploying, managing, debugging lambda function,...
#### install serverless
-   config serverless credential
```
serverless config credentials --provider aws --key A... --secret 4Mi... profile serverless-admin
```
-   successfull: Profile "default" has been configured
-   checking config
```
cat ~/.aws/credentials
```

### 3.8
-   create project serverless:
```
serverless
serverless create --help
```
```
sls create --template google-nodejs --path hello-world-nodejs
```
-   sync code or deploy specify func
```
sls deploy
```

### 3.9
```
sls deploy function -f hello
```

### 3.10
-   fetch and log
```
sls invoke -f hello
sls invoke -f hello --log
sls logs -f hello
```

### 3.11
-   clean up
```
sls remove
```

## section 4
### 4.13

### 4.14
-   array yaml:
```
numbers:
  - one
  - two
  - three
or
numbers: ["one","two","three"]
```

-   object yaml:
```
author:
  name: duc
  age: 24
```

-   list items:
```
items:
  - 1:
    author: duc
  - 2:
    author: duc1
```

-   reference:
```
author: &jD
  name: duc
  last_name: nguyen

# list item
items:
  - 1:
    author: *jD
  - 2:
    author: *jD
```

### 4.15
-   Function
  - single job: store user in dynamoDB, convert image into thumbnail, exec job only in your function
  - deploy code in cloud

-   Events
  - trigger lambda function to execute: upload image to s3, aws gateway

-   Resources
  - infrastructure component which your function used: store data into dynamoDB, audio file, send message async

-   Services
  - include: project file, resource your function used

### 4.16
-   setup function timeout and memory
```failed
{
  "errorMessage": "2022-07-26T06:10:37.940Z 9ef18f09-0453-4ab8-a812-02428d766fd2 Task timed out after 3.01 seconds"
}
```

### 4.19
-   environment variables: set env for application, set global env in block environment, or overide env for each application -> set environment for app in block function

### 4.20
-   vpc: router traffic from one vpc to another vpc using private ip address, users are able to connect to more than one vpc at the same time

### 4.21
-   Lambda Pricing: 
pay per calls:
  first 1,000,000 requests are free
  $0.20 per 1 miliion requests == $0.0000002 per request
pay per duration:
  400,000 GB-seconds of compute time per month free
  == 400000 second if function is 1GB RAM
  == 320000 second if function is 128MB RAM

## section 5
-   setup aws toolkit, awscli, aws-credentials 
-   get aws info
```
aws sts get-caller-identity
aws configure
```

-   install aws cli
```
https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install-linux.html
```

## section 6
### 6.28
-   step by step
  - sam init
  - sam build => build succeeded
  - sam local invoke => response data the same lamdba function is like webbrower

### 6.29
-   deploy
-   run
```
sum deploy --guide
```
+   Confirm changes before deploy: Y
+   Deploy this changeset: Y
+   Allow SAM CLI IAM role creation [Y/n]: y
+   Disable rollback [y/N]: n
+   HelloWorldFunction may not have authorization defined, Is this okay? [y/N]: y
+   Save arguments to configuration file [Y/n]: y

-   Output:
```
Outputs                                                                                                                                                                                                  
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
Key                 HelloWorldFunctionIamRole                                                                                                                                                            
Description         Implicit IAM Role created for Hello World function                                                                                                                                   
Value               arn:aws:iam::109549956429:role/sam-app-HelloWorldFunctionRole-O0XMB3S5AX9T                                                                                                           

Key                 HelloWorldApi                                                                                                                                                                        
Description         API Gateway endpoint URL for Prod stage for Hello World function                                                                                                                     
Value               https://9l5nq1gude.execute-api.us-east-1.amazonaws.com/Prod/hello/                                                                                                                   

Key                 HelloWorldFunction                                                                                                                                                                   
Description         Hello World Lambda Function ARN                                                                                                                                                      
Value               arn:aws:lambda:us-east-1:109549956429:function:sam-app-HelloWorldFunction-TEt5vw5NVc7N
```

-   copy url in block HelloWorldAPI and open web
```
https://9l5nq1gude.execute-api.us-east-1.amazonaws.com/Prod/hello/
or
curl https://9l5nq1gude.execute-api.us-east-1.amazonaws.com/Prod/hello/
```

### 6.30
-   run
```
sam local start-api
```

-   change code => run
```
sam build
sam local start-api
```

### 6.31
-   invoke directly lambda function
```
sam local invoke "HelloWorldFunction" -e events/event.json
```

### 6.32
-   deleting the stack
```
aws cloudformation delete-stack --stack-name sam-app --region us-east-1
```

## section 7
### 7.34
-   create sam-app with vsc
-   Debug in vsc, output:
```
Debugger attached.
END RequestId: 897af443-b230-4273-a495-4c17d3296b27
REPORT RequestId: 897af443-b230-4273-a495-4c17d3296b27	Init Duration: 2.25 ms	Duration: 5461.58 ms	Billed Duration: 5462 ms	Memory Size: 128 MB	Max Memory Used: 128 MB	
No Content-Type given. Defaulting to 'application/json'.
2022-07-27 09:21:43 127.0.0.1 - - [27/Jul/2022 09:21:43] "GET /hello HTTP/1.1" 200 -
```

## section 8
### 8.38
-   what is step function: allows developer build visual workflows for business processes, automated environment, checking if username and email valid, if so then allow users to open a new account, basic on state machines and stask
-   benefits: build and deploy fast, 

### 8.39
-   Step function: 
-   wait state: deplay the state machine, ex: Seconds Timestamp SecondsPath TimestampPath
-   choice state: nameFood dinnerchoice

## section 9
### 9.44
-   step function and state machine with VSC

### 9.45
-   create new step function in VSC:
  + create new step fuction
  + render state machine graph
  + public state machine cloud



## section 12
-   Rest api with nodejs
```
sls
```
=> What org do you want to add this service to? [Skip]
=> Do you want to deploy now? No

  + run:
```
npm install
npm install uuid
```

-   exec function serverless local
```
sls invoke local --function create
```

-   deploy
```
sls deploy
```

## example Golang
-   create project:
```
sls create -t aws-go -p abc
sls create -t aws-go-dep -p abc
```

-   exec local
```
sls invoke local -f world
```

-   deploy
```
sls deploy
```