package logger

import (
	"flag"
	"go/build"
	"log"
	"os"
	"runtime"
	"sync"
)

const (
	VERSION = "0.0.1"
)

var (
	LogWriter *log.Logger
	once      sync.Once
)

func InitLogger() {
	once.Do(func() {
		var logPath = build.Default.GOPATH + "/info.log"
		flag.Parse()
		var file, err = os.Create(logPath)
		if err != nil {
			panic(err)
		}
		LogWriter = log.New(file, "", log.LstdFlags|log.Lshortfile)
	})
}

func MainLoggerWriter() {
	LogWriter.Printf("Server v%s pid=%d started with processes: %d", VERSION, os.Getpid(), runtime.GOMAXPROCS(runtime.NumCPU()))
}
