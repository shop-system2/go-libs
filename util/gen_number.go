package util

import (
	"encoding/base64"
	"encoding/hex"
	"fmt"

	"math/rand"
	"strings"
	"time"
)

var letters = []rune("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

var lettersOTP = []rune("0123456789")

func randString(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

func randStringOTP(n int) string {
	rand.Seed(time.Now().UnixNano())
	b := make([]rune, n)
	for i := range b {
		b[i] = lettersOTP[rand.Intn(len(lettersOTP))]
	}
	return string(b)
}

func checkSpecialNumber(input string) bool {
	arrayInput := strings.Split(input, "")
	checkMap := make(map[string]int, len(arrayInput))
	counter := 0
	for i := 0; i < len(arrayInput); i++ {
		if _, ok := checkMap[arrayInput[i]]; ok {
			counter++
		}
		fmt.Println("counter", counter)
		checkMap[arrayInput[i]] = i
		if counter > (len(arrayInput)/2 - 1) {
			return false
		}
	}
	return true
}

func GenOTP(lengthOTP int) string {
	var (
		otp        string
		specialOTP bool
	)
	cou := 0
	for {
		fmt.Println("run")
		otp = randStringOTP(lengthOTP)
		specialOTP = checkSpecialNumber(otp)
		cou++
		if specialOTP {
			break
		}
	}
	return otp
}

//GetID represents get unique id for transaction
func GetID() string {
	dest, _ := hex.DecodeString(fmt.Sprintf("%d", nowAsUnixSecond()))
	var id strings.Builder
	encode := base64.StdEncoding.EncodeToString(dest)
	rand.Seed(time.Now().UnixNano())
	id.WriteString(encode)
	id.WriteString(randString(4))
	return strings.Replace(id.String(), "=", randString(1), 1)
}

func nowAsUnixSecond() int64 {
	return time.Now().UnixNano() / 1e9
}
