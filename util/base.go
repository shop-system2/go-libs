package util

import (
	"context"

	"google.golang.org/grpc/metadata"
)

func UserInMD(ctx context.Context) string {
	md, ok := metadata.FromIncomingContext(ctx)
	if ok {
		domain, ok := md["domain"]
		if ok && len(domain) != 0 {
			return domain[0]
		}
	}
	return ""
}

// type visitor struct {
// 	limiter  *rate.Limiter
// 	lastSeen time.Time
// }

// var visitors = make(map[string]*visitor)
// var mu sync.Mutex

// func GetVisitor(channelType string) *rate.Limiter {
// 	mu.Lock()
// 	defer mu.Unlock()
// 	v, exists := visitors[channelType]
// 	if !exists {
// 		fmt.Println("new request")
// 		rateRequest := rate.Every(120 * time.Second / 3)
// 		limiter := rate.NewLimiter(rateRequest, 2)
// 		// Include the current time when creating a new visitor.
// 		visitors[channelType] = &visitor{limiter, time.Now()}
// 		return limiter
// 	}
// 	fmt.Println("request exists")

// 	// Update the last seen time for the visitor.
// 	v.lastSeen = time.Now()
// 	return v.limiter
// }

// func CleanupVisitors(channelType string) {
// 	for {
// 		time.Sleep(time.Minute)

// 		mu.Lock()
// 		for ip, v := range visitors {
// 			if time.Since(v.lastSeen) > 3*time.Minute {
// 				delete(visitors, ip)
// 			}
// 		}
// 		mu.Unlock()
// 	}
// }
