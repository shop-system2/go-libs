### Design Pattern

#### Command Design Pattern

Trong lập trình hướng đối tượng, command design pattern là behaviroal, nó để gói lại tất cả các thông tin cũng như action trên các logic khác nhau, bao gồm như tên biến tên function và parameter của function.

Command design pattern có 4 điều khoản được liên kết với nhau:
    command
    receiver
    invoker
    client
- A command được biết về việc nhận và thực thi các method của việc nhận. Giá trị các biến của phương thức nhận được lưu trong các command.
- The receiver là object để thực thi những phương thức và cũng được store trong command thông qua command object bởi aggregation(cái này sẽ giải thích sau), receiver được thực hiện khi execute() method trong command được gọi.
- The invoker object được biết là thực hiện execute() như thế nào, nó được xem như một người kế toán để thực thi các command và chỉ biết về các command interface(interface thế nào thì các bạn đọc thêm nhé, hoặc cần mình sẽ giải thích sau nè)
- The client là quyết định sẽ thực thi tại điểm nào, command nào và pass command object đến invoker object

Tóm gọn lại thì nếu không dùng command design pattern là khi một button mà có nhiều action kiểu như hình.


thì như bình thường ta phải viết mỗi sub class để xử lý logic cho tưng logic khác nhau thì ta app dụng command design pattern.


Mình sẽ ví dụ code trên ngôn ngữ golang: 
- Đầu tiê

