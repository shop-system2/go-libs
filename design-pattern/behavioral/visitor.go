package behavioral

import "fmt"

type (
	square struct {
		side int
	}

	rectangle struct {
		l int
		b int
	}

	circle struct {
		radius int
	}

	visitor interface {
		visitForSquare(*square)
		visitForRectangle(*rectangle)
		visitForCircle(*circle)
	}

	shape interface {
		accept(visitor)
		getType() string
	}
)

func (s *square) accept(v visitor) {
	fmt.Println("qqqqqqqqqqqq")
	v.visitForSquare(s)
}

func (s *square) getType() string {
	return "Square"
}

func (s *rectangle) accept(v visitor) {
	v.visitForRectangle(s)
}

func (s *rectangle) getType() string {
	return "Rectangle"
}

func (c *circle) accept(v visitor) {
	v.visitForCircle(c)
}

func (c *circle) getType() string {
	return "Circle"
}

// demo
type demo struct {
	area int
}

func (a *demo) visitForCircle(v *circle) {
	fmt.Println("demo Circle")
}

func (a *demo) visitForRectangle(v *rectangle) {
	fmt.Println("demo Rectangle")
}

func (a *demo) visitForSquare(v *square) {
	fmt.Println("demo Square")
}
func init() {
	square := &square{side: 2}
	// circle := &circle{radius: 3}
	// rectangle := &rectangle{l: 2, b: 3}

	demo := &demo{area: 1}

	square.accept(demo)
	// circle.accept(demo)
	// rectangle.accept(demo)

}

func main() {
}
