package behavioral

import (
	"fmt"
)

type List []string

type Task struct {
	list List
}

func NewTask() *Task {
	return &Task{}
}
func (t *Task) Add(s string) {
	t.list = append(t.list, s)
}

func (t *Task) All() List {
	return t.list
}

type Memento struct {
	list List
}

func (m *Memento) List() List {
	return m.list
}

func (t *Task) Memento() Memento {
	return Memento{
		list: t.list,
	}
}
func (t *Task) Restore(m Memento) {
	t.list = m.list
}

type History struct {
	history []Memento
}

func NewHistory() *History {
	return &History{
		make([]Memento, 0),
	}
}

func (h *History) Save(m Memento) {
	h.history = append(h.history, m)
}

func (h *History) Undo() Memento {

	if len(h.history) > 1 {
		n := len(h.history) - 1
		h.history = h.history[:n]
		return h.history[len(h.history)-1]
	} else {
		return Memento{}

	}

}

func MainMementoPattern() {
	fmt.Println("behavioral design pattern ")
	history := NewHistory()

	task := NewTask()

	task.Add("task 1")
	history.Save(task.Memento())

	task.Add("task 2")
	history.Save(task.Memento())

	fmt.Println("111:", task.All())

	task.Restore(history.Undo())
	fmt.Println("222:", task.All())

	task.Restore(history.Undo())
	fmt.Println("333:", task.All())
}
