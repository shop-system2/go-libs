package influxdb

// import (
// 	"encoding/json"
// 	"fmt"
// 	"html/template"
// 	"log"
// 	"net/http"
// 	"os"
// 	"path"
// 	"time"

// 	"errors"
// 	"log"
// 	"time"

// 	"github.com/influxdata/influxdb/client/v2"
// )

// // Product structure of my cool shop product
// type Product struct {
// 	ID          int     `json:"id"`
// 	Name        string  `json:"name"`
// 	Price       float32 `json:"price"`
// 	Image       string  `json:"image"`
// 	Description string  `json:"description"`
// 	Views       string
// }

// // ProductMeasurement schema of product measurement
// type ProductMeasurement struct {
// 	ProductID   int `json:"id"`
// 	ProductName string
// 	time        time.Time
// }

// var allProducts = []Product{
// 	Product{ID: 0, Name: "Watch", Price: 56.2, Image: "/assets/img/watch.jpeg", Description: "Nice Watch"},
// 	Product{ID: 1, Name: "Camera", Price: 34.2, Image: "/assets/img/camera.jpeg", Description: "Nice Camera"},
// 	Product{ID: 2, Name: "Glass", Price: 24.2, Image: "/assets/img/glass.jpeg", Description: "Nice Glass"},
// 	Product{ID: 3, Name: "Toy", Price: 56.2, Image: "/assets/img/toy.jpeg", Description: "Nice Toy"},
// }

// // GetAll fetches all products in my shop
// func (product Product) GetAll() []Product {
// 	return allProducts
// }

// // Get fetches a particular product in my shop identified By its ID
// func (product Product) Get(id int) (Product, error) {
// 	if id < 0 && len(allProducts) >= id {
// 		log.Println(id)
// 		return Product{}, errors.New("No product found")
// 	}
// 	return allProducts[id], nil
// }

// const (
// 	// MyDB specifies name of database
// 	MyDB = "covid19"
// 	URL  = "http://172.29.164.163:8086"
// )

// func MainInfluxDB() {
// 	GetProduct(1)

// 	r.Get("/", shops.GetAllProducts)
// 	r.Get("/product/{ID}", shops.GetProduct)
// }

// func Insert(productMeasurement map[string]interface{}) {
// 	c, err := client.NewHTTPClient(client.HTTPConfig{
// 		Addr: "http://localhost:8086",
// 	})
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	defer c.Close()

// 	// Create a new point batch
// 	bp, err := client.NewBatchPoints(client.BatchPointsConfig{
// 		Database:  MyDB,
// 		Precision: "s",
// 	})
// 	if err != nil {
// 		log.Fatal(err)
// 	}

// 	// Create a point and add to batch
// 	tags := map[string]string{"productView": productMeasurement["ProductName"].(string)}
// 	fields := productMeasurement

// 	pt, err := client.NewPoint("products", tags, fields, time.Now())
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	bp.AddPoint(pt)

// 	// Write the batch
// 	if err := c.Write(bp); err != nil {
// 		log.Fatal(err)
// 	}

// 	// Close client resources
// 	if err := c.Close(); err != nil {
// 		log.Fatal(err)
// 	}
// }

// func queryDB(cmd string) (res []client.Result, err error) {
// 	q := client.Query{
// 		Command:  cmd,đ
// 		Database: MyDB,
// 	}
// 	c, err := client.NewHTTPClient(client.HTTPConfig{
// 		Addr: URL,
// 	})
// 	if err != nil {
// 		return res, err
// 	}
// 	response, err := c.Query(q)
// 	if err == nil {
// 		if response.Error() != nil {
// 			return res, response.Error()
// 		}
// 		res = response.Results
// 	} else {
// 		fmt.Println("error ", err)
// 	}
// 	return res, nil
// }

// // GetAllProducts fetches all products in my shop
// func GetAllProducts(w http.ResponseWriter, r *http.Request) {
// 	t := template.New("index.html") // Create a template.
// 	cwd, _ := os.Getwd()
// 	p := path.Join(cwd, "public", "index.html")
// 	t, err := t.ParseFiles(p) // Parse template file.
// 	if err != nil {
// 		log.Println(err)
// 	}
// 	products := Product{}.GetAll() // Get current user infomration.
// 	err = t.Execute(w, products)   // merge.
// 	if err != nil {
// 		log.Println(err)
// 	}
// }

// // GetProduct handler for fetching a particular product
// func GetProduct(w http.ResponseWriter, r *http.Request) {
// 	// ID, err := strconv.Atoi(chi.URLParam(r, "ID"))
// 	// if err != nil {
// 	// 	log.Println(err)
// 	// 	return
// 	// }
// 	ID := 1
// 	product, err := Product{}.Get(ID)
// 	if err != nil {
// 		log.Println(err)
// 	}
// 	t := template.New("product.html") // Create a template.
// 	cwd, _ := os.Getwd()
// 	p := path.Join(cwd, "public", "product.html")
// 	t, err = t.ParseFiles(p) // Parse template file.
// 	if err != nil {
// 		log.Println(err)
// 	}
// 	res, err := queryDB(fmt.Sprintf("select count(ProductID) from products where ProductID = %d", product.ID))
// 	if err != nil {
// 		log.Println(err)
// 	}
// 	result := res[0]
// 	if len(result.Series) > 0 {
// 		product.Views = string(result.Series[0].Values[0][1].(json.Number))
// 	} else {
// 		product.Views = "0"
// 	}
// 	err = t.Execute(w, product) // merge.
// 	if err != nil {
// 		log.Println(err)
// 	}

// 	pm := &ProductMeasurement{product.ID, product.Name, time.Now()}

// 	Insert(structs.Map(pm))
// }
