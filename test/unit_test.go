package test

import (
	"testing"
)

func sum(x, y int) int {
	return x + y
}

// func OK
// func TestSum(t *testing.T) {
// 	total := sum(5, 5)
// 	if total != 10 {
// 		t.Errorf("Sum was incorrect, got: %d, want: %d.", total, 10)
// 	}
// }

func TestSumTable(t *testing.T) {
	tables := []struct {
		x int
		y int
		n int
	}{
		{1, 1, 2},
		{1, 2, 3},
		{2, 2, 4},
		{5, 2, 7},
	}
	for _, table := range tables {
		total := sum(table.x, table.y)
		if total != table.n {
			t.Errorf("Sum of (%d+%d) was incorrect, got: %d, want: %d.", table.x, table.y, total, table.n)
		}
	}
}

// go test
// go test -v
// go test -cover
// go test -cover -coverprofile=c.out
// go tool cover -html=c.out -o coverage.html
