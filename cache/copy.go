package cache

import (
	"fmt"
	"log"
	"os"
	"time"

	"github.com/mediocregopher/radix/v3"
	"github.com/spf13/cobra"
)

var (
	pattern                  string
	scanCount, limit, report int
)

var copyCmd = &cobra.Command{
	Use:   "copy [sourceHost:port] [targetHost:port]",
	Short: "Copy keys from source redis intance to destication by given pattern",
	Long:  ``,
	Args:  cobra.MinimumNArgs(0),

	Run: func(cmd *cobra.Command, args []string) {
		start := time.Now()
		fmt.Println("Start copying")
		fmt.Println(cmd)
		fmt.Println(args)
		clientSource, err := radix.DefaultClientFunc("tcp", args[0])
		if err != nil {
			log.Fatal(err)
		}
		clientTarget, err := radix.DefaultClientFunc("tcp", args[1])
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println(clientSource, clientTarget)

		scanOpts := radix.ScanOpts{
			Command: "SCAN",
			Count:   scanCount,
		}

		if pattern != "*" {
			scanOpts.Pattern = pattern
		}

		scanner := radix.NewScanner(clientSource, scanOpts)

		var key string
		counter := 0
		cycle := 0
		cycleStart := time.Now()
		for scanner.Next(&key) {

			var value string
			var ttl int

			p := radix.Pipeline(
				radix.Cmd(&ttl, "PTTL", key),
				radix.Cmd(&value, "DUMP", key),
			)

			if err := clientSource.Do(p); err != nil {
				panic(err)
			}

			if ttl < 0 {
				ttl = 0
			}

			err = clientTarget.Do(radix.FlatCmd(nil, "RESTORE", key, ttl, value, "REPLACE"))
			if err != nil {
				log.Fatal(err)
			}

			counter++
			cycle++

			if cycle == report {
				log.Printf("Copied another %d keys in: %s", report, time.Since(cycleStart))
				cycle = 0
				cycleStart = time.Now()
			}

			if limit > 0 && counter > limit {
				fmt.Printf("Early exit after %d keys copied\n", counter)
				return
			}
		}

		if err := scanner.Close(); err != nil {
			log.Fatal(err)
		}

		log.Printf("In total %d keys copied in %s", counter, time.Since(start))

	},
}

var rootCmd = &cobra.Command{
	Use:   "go-redis-migrate",
	Short: "Application to migrate redis data from one instance to another",
	Long:  "",
}

// https://github.com/obukhov/go-redis-migrate
func init() {
	fmt.Println("MainCopyRedis")

	rootCmd.AddCommand(copyCmd)
	copyCmd.Flags().StringVar(&pattern, "pattern", "*", "Match pattern for keys")
	copyCmd.Flags().IntVar(&scanCount, "scanCount", 100, "COUNT parameter for redis SCAN command")
	copyCmd.Flags().IntVar(&report, "report", 1000, "After what number of keys copied to report time")
	copyCmd.Flags().IntVar(&limit, "limit", 0, "After what number of keys copied to stop (0 - unlimited)")

	fmt.Println("refinish")
}

func Excute() {
	err := rootCmd.Execute()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

// go-redis-migrate copy 127.0.0.1:6380 127.0.0.1:6381 --pattern="prefix:*"
