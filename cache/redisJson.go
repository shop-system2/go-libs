package cache

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"time"

	goredis "github.com/go-redis/redis/v8"

	"github.com/nitishm/go-rejson/v4"
)

func ExampleJsonGet(rh *rejson.Handler) {
	startNow := time.Now()

	readStudent := StudentResponse{}
	getResp, err := rh.JSONGet("student-1", ".")
	if err != nil {
		log.Fatalf("get failed %s", err)
		return
	}
	sdf := getResp.([]byte)
	err = json.Unmarshal(sdf, &readStudent)
	if err != nil {
		log.Fatalf("Failed to JSON Unmarshal")
		return
	}
	fmt.Println("microsecond time get:", time.Since(startNow).Microseconds())
	fmt.Printf("Student read from redis : %#v \n", readStudent)
}

func Example_JSONSet(rh *rejson.Handler) {
	student := initStruct()
	fmt.Println(student)

	startNow := time.Now()
	for i := 0; i < 1000; i++ {
		fmt.Println(i)
		student.Identify = i
		key := fmt.Sprintf("%s-%d", "student", i)
		res, err := rh.JSONSet(key, ".", student)
		if err != nil {
			fmt.Println(err)
			log.Fatalf("Failed to JSONSet")
			return
		}
		if res.(string) != "OK" {
			fmt.Println("Failed to Set: ")
		}
	}

	fmt.Println("millisecond time set:", time.Since(startNow).Milliseconds())

	log.Default().Println("done set redis")
}

func MainRedisJson(ctx context.Context) {

	var addr = flag.String("Server", "localhost:6379", "Redis server address")

	rh := rejson.NewReJSONHandler()
	flag.Parse()

	// Redigo Client

	// GoRedis Client
	cli := goredis.NewClient(&goredis.Options{Addr: *addr})
	rh.SetGoRedisClient(cli)
	// Example_JSONSet(rh)
	// ExampleJsonGet(rh)
}

func initStruct() StudentResponse {
	return StudentResponse{
		IsBuyNow:                false,
		UseOrderNotes:           false,
		DiscountAmount:          1,
		SellerCouponUsed:        []string{"abc", "123"},
		IsAutoApplySellerCoupon: false,
		AbleToBeSentAsGift:      false,
		ID:                      "1231231231231",
		LastPaymentMethod: LastPaymentMethod{
			IconURL:           "http:google.com",
			Method:            "",
			SelectedToken:     "",
			MethodName:        "",
			SelectedBank:      "",
			HasPromotion:      false,
			IsPromotionMethod: false,
			IsLinkedMomo:      true,
		},
		IsSentAsGift:         true,
		HasShoppingCartGifts: false,
		VatNoticeLink:        "",
		ShippingMethods: []ShippingMethods{
			ShippingMethods{
				ID:                  123,
				Name:                "",
				Code:                "",
				EstimationText:      "",
				Description:         "",
				OrgShippingFee:      123,
				ShippingFee:         123,
				DiscountShippingFee: 123,
				IsDisable:           false,
				IsMultiple:          true,
				ShowShipments:       true,
				IsSelected:          false,
				Disable:             false,
				Multiple:            true,
				Selected:            false,
				ShippingBadgeOption: ShippingBadgeOption{
					Name:        "name",
					Description: "des",
					Value:       "value",
				},
			},
		},
		CustomerReward: CustomerReward{
			Total:      123,
			Used:       123,
			Applicable: 23423,
			IsValid:    123,
		},
		Subtotal:                  1,
		IsVat:                     false,
		HandlingFee:               12,
		ResidentialMessage:        "",
		TikixuPointEarning:        123,
		IsFollowerCouponAvailable: false,
		TotalDiscount:             132,
		CheckoutTrackingInfo: CheckoutTrackingInfo{
			AstraFreeshipSource:        "",
			AstraTotalCashbackAmount:   123,
			DeliveryAfterDiscountTotal: 312321,
			AstraFreeshipAmount:        32452,
			DeliveryDiscountTotal:      32423,
			AstraAmountUse:             242,
			AstraCashbackRemaining:     12,
			DeliveryFeeTotal:           23432,
		},
		PriceSummary: []PriceSummary{},
		AsaInfo: AsaInfo{
			AsaTikixuEquiv: 123,
			AsaDisclaimers: []string{"sdfsd", "s"},
			AsaFreeshipWidget: AsaFreeshipWidget{
				Subtitle:       "sub asta",
				DisabledReason: "reason asta",
				Title:          "title asta",
				IsUseAsa:       false,
				IsEnableAsa:    true,
			},
		},
		IsEnableHnb:        false,
		GrandTotal:         1811,
		CouponUsedQuantity: 123,
		ShippingAddress: ShippingAddress{
			Country:             "viet nam",
			DeliveryAddressType: "home",
			City:                "",
			RegionID:            123,
			AddressID:           "",
			WardTikiCode:        "",
			Telephone:           "",
		},
		ImportTaxInfo:              "",
		PlatformCouponUsed:         []string{"sdfsd", "s"},
		UnsupportedShippingMethods: []string{"sdfsd", "s"},
		CouponCode:                 "123sdf",
		OrgShippingAmount:          222,
		FulfillmentDetails:         "sfds12",
		VatNotice:                  []string{"abc", "vsdf"},
		FulfillmentWarningMessage:  "",
		InstallationFee:            123,
		GiftCardAmount:             32423,
		ShippingAmount:             312,
		DiscountShippingAmount:     432,
		LeaveAtDoorInfo: LeaveAtDoorInfo{
			IsEnable: true,
			Subtitle: " LeaveAtDoorInfo sub",
			Title:    "LeaveAtDoorInfo",
		},
		ShippingPlan: ShippingPlan{
			ShippingFee:         123123,
			DiscountShippingFee: 123,
			Code:                "abcss",
			OrgShippingFee:      42,
			Name:                "ship name",
			ID:                  12312321,
		},
		CustomerID: 123123123,
		Messages: []Messages{
			Messages{
				Message:   "",
				Type:      "",
				ErrorCode: 123,
			},
			Messages{
				Message:   "",
				Type:      "",
				ErrorCode: 123,
			},
		},
	}
}

type LeaveAtDoorInfo struct {
	IsEnable bool   `json:"is_enable"`
	Subtitle string `json:"subtitle"`

	Title string `json:"title"`
}
type ShippingPlan struct {
	ShippingFee         int    `json:"shipping_fee"`
	DiscountShippingFee int    `json:"discount_shipping_fee"`
	Code                string `json:"code"`
	OrgShippingFee      int    `json:"org_shipping_fee"`
	Name                string `json:"name"`
	ID                  int    `json:"id"`
}
type StudentResponse struct {
	Identify                   int                  `json:"identidy"`
	IsBuyNow                   bool                 `json:"is_buy_now"`
	UseOrderNotes              bool                 `json:"use_order_notes"`
	DiscountAmount             int                  `json:"discount_amount"`
	SellerCouponUsed           []string             `json:"seller_coupon_used"`
	IsAutoApplySellerCoupon    bool                 `json:"is_auto_apply_seller_coupon"`
	AbleToBeSentAsGift         bool                 `json:"able_to_be_sent_as_gift"`
	ID                         string               `json:"id"`
	LastPaymentMethod          LastPaymentMethod    `json:"last_payment_method"`
	IsSentAsGift               bool                 `json:"is_sent_as_gift"`
	HasShoppingCartGifts       bool                 `json:"has_shopping_cart_gifts"`
	VatNoticeLink              string               `json:"vat_notice_link"`
	ShippingMethods            []ShippingMethods    `json:"shipping_methods"`
	CustomerReward             CustomerReward       `json:"customer_reward"`
	Subtotal                   int                  `json:"subtotal"`
	IsVat                      bool                 `json:"is_vat"`
	HandlingFee                int                  `json:"handling_fee"`
	ResidentialMessage         string               `json:"residential_message"`
	TikixuPointEarning         int                  `json:"tikixu_point_earning"`
	IsFollowerCouponAvailable  bool                 `json:"is_follower_coupon_available"`
	TotalDiscount              float64              `json:"total_discount"`
	CheckoutTrackingInfo       CheckoutTrackingInfo `json:"checkout_tracking_info"`
	PriceSummary               []PriceSummary       `json:"price_summary"`
	AsaInfo                    AsaInfo              `json:"asa_info"`
	IsEnableHnb                bool                 `json:"is_enable_hnb"`
	GrandTotal                 int                  `json:"grand_total"`
	CouponUsedQuantity         int                  `json:"coupon_used_quantity"`
	ShippingAddress            ShippingAddress      `json:"shipping_address"`
	ImportTaxInfo              string               `json:"import_tax_info"`
	PlatformCouponUsed         []string             `json:"platform_coupon_used"`
	UnsupportedShippingMethods []string             `json:"unsupported_shipping_methods"`
	CouponCode                 string               `json:"coupon_code"`
	OrgShippingAmount          int                  `json:"org_shipping_amount"`
	FulfillmentDetails         string               `json:"fulfillment_details"`
	VatNotice                  []string             `json:"vat_notice"`
	FulfillmentWarningMessage  string               `json:"fulfillment_warning_message"`
	InstallationFee            int                  `json:"installation_fee"`
	GiftCardAmount             int                  `json:"gift_card_amount"`
	ShippingAmount             int                  `json:"shipping_amount"`
	DiscountShippingAmount     int                  `json:"discount_shipping_amount"`
	LeaveAtDoorInfo            LeaveAtDoorInfo      `json:"leave_at_door_info"`
	ShippingPlan               ShippingPlan         `json:"shipping_plan"`
	CustomerID                 int                  `json:"customer_id"`
	Messages                   []Messages           `json:"messages"`
}

type ShippingAddress struct {
	Country             string      `json:"country"`
	DeliveryAddressType string      `json:"delivery_address_type"`
	City                string      `json:"city"`
	RegionID            int         `json:"region_id"`
	AddressID           string      `json:"address_id"`
	WardTikiCode        string      `json:"ward_tiki_code"`
	Telephone           string      `json:"telephone"`
	Ward                string      `json:"ward"`
	CityTikiCode        string      `json:"city_tiki_code"`
	WarningMessage      interface{} `json:"warning_message"`
	FullName            string      `json:"full_name"`
	RegionTikiCode      string      `json:"region_tiki_code"`
	Street              string      `json:"street"`
	Name                string      `json:"name"`
	DistrictID          int         `json:"district_id"`
	Region              string      `json:"region"`
	WardID              int         `json:"ward_id"`
	CityID              int         `json:"city_id"`
}

// LastPaymentMethod ...
type LastPaymentMethod struct {
	IconURL       string `json:"icon_url"`
	Method        string `json:"method"`
	SelectedToken interface {
	} `json:"selected_token"`
	MethodName   string `json:"method_name"`
	SelectedBank interface {
	} `json:"selected_bank"`
	HasPromotion      bool `json:"has_promotion"`
	IsPromotionMethod bool `json:"is_promotion_method"`
	IsLinkedMomo      bool `json:"is_linked_momo"`
}

// PriceSummary...
type PriceSummary struct {
	Name       string      `json:"name"`
	Status     string      `json:"status"`
	Value      string      `json:"value"`
	NameStatus interface{} `json:"name_status"`
}

// CustomerReward ...
type CustomerReward struct {
	Total      int `json:"total"`
	Used       int `json:"used"`
	Applicable int `json:"applicable"`
	IsValid    int `json:"is_valid"`
}

type ShippingBadgeOption struct {
	Name        interface{} `json:"name"`
	Description interface{} `json:"description"`
	Value       string      `json:"value"`
}

// ShippingMethods ...
type ShippingMethods struct {
	ID                  int                 `json:"id"`
	Name                string              `json:"name"`
	Code                string              `json:"code"`
	EstimationText      string              `json:"estimation_text"`
	Description         string              `json:"description"`
	OrgShippingFee      int                 `json:"org_shipping_fee"`
	ShippingFee         int                 `json:"shipping_fee"`
	DiscountShippingFee int                 `json:"discount_shipping_fee"`
	IsDisable           bool                `json:"is_disable"`
	IsMultiple          bool                `json:"is_multiple"`
	ShowShipments       bool                `json:"show_shipments"`
	IsSelected          bool                `json:"is_selected"`
	Disable             bool                `json:"disable"`
	Multiple            bool                `json:"multiple"`
	Selected            bool                `json:"selected"`
	ShippingBadgeOption ShippingBadgeOption `json:"shipping_badge_option"`
}

//CheckoutTrackingInfo...
type CheckoutTrackingInfo struct {
	AstraFreeshipSource        string  `json:"astra_freeship_source"`
	AstraTotalCashbackAmount   float64 `json:"astra_total_cashback_amount"`
	DeliveryAfterDiscountTotal int     `json:"delivery_after_discount_total"`
	AstraFreeshipAmount        int     `json:"astra_freeship_amount"`
	DeliveryDiscountTotal      int     `json:"delivery_discount_total"`
	AstraAmountUse             float64 `json:"astra_amount_use"`
	AstraCashbackRemaining     float64 `json:"astra_cashback_remaining"`
	DeliveryFeeTotal           int     `json:"delivery_fee_total"`
}

//AsaSummary...
type AsaSummary struct {
	Name       string      `json:"name"`
	Status     string      `json:"status"`
	Value      string      `json:"value"`
	NameStatus interface{} `json:"name_status"`
}

//AsaFreeshipWidget...
type AsaFreeshipWidget struct {
	Subtitle       string `json:"subtitle"`
	DisabledReason string `json:"disabled_reason"`
	Title          string `json:"title"`
	IsUseAsa       bool   `json:"is_use_asa"`
	IsEnableAsa    bool   `json:"is_enable_asa"`
}

//AsaInfo...
type AsaInfo struct {
	AsaTikixuEquiv    int               `json:"asa_tikixu_equiv"`
	AsaDisclaimers    []string          `json:"asa_disclaimers"`
	AsaFreeshipWidget AsaFreeshipWidget `json:"asa_freeship_widget"`
	AsaSummary        []AsaSummary      `json:"asa_summary"`
}

//Message...
type Messages struct {
	Message   string `json:"message"`
	Type      string `json:"type"`
	ErrorCode int    `json:"error_code"`
}

//TimeSlots ...
type TimeSlots struct {
	IsAvailable bool   `json:"is_available"`
	TimeZone    string `json:"time_zone"`
	ID          int    `json:"id"`
	EndTime     string `json:"end_time"`
	IsSelected  bool   `json:"is_selected"`
	StartTime   string `json:"start_time"`
}

//Slots ...
type Slots struct {
	TimeSlots  []TimeSlots `json:"time_slots"`
	Date       string      `json:"date"`
	DateOfWeek string      `json:"date_of_week"`
}

//GetCartMineInfoRequest...
type GetCartMineInfoRequest struct {
	// Metadata Metadata `json:"metadata"`
	Include string `json:"include" url:"include" query:"include"`
}
