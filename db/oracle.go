package db

import (
	"database/sql"
	"fmt"

	_ "github.com/sijms/go-ora/v2"
)

type dbPkg struct {
	db *sql.DB
}

func InitOracle(host, port, username, passsword, database string) DBHelper {
	connectionString := "oracle://" + username + ":" + passsword + "@" + host + ":" + port + "/" + database
	db, err := sql.Open("oracle", connectionString)
	if err != nil {
		panic(fmt.Errorf("error in sql.Open: %w", err))
	}
	if err := db.Ping(); err != nil {
		panic(fmt.Errorf("error pinging db: %w", err))
	}
	return &dbPkg{
		db: db,
	}
}

func (h *dbPkg) Open() *sql.DB {
	return h.db
}

func (h *dbPkg) Close() error {
	return h.db.Close()
}

func (h *dbPkg) Begin() (*sql.Tx, error) {
	return h.db.Begin()
}

func (h *dbPkg) Commit(tx *sql.Tx) error {
	return tx.Commit()
}

func (h *dbPkg) Rollback(tx *sql.Tx) error {
	return tx.Rollback()
}
