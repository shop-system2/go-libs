package hash

import (
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"errors"
	"fmt"
)

func GenerateKeyRSA(bits int) (*rsa.PrivateKey, *rsa.PublicKey, error) {
	var (
		privateKey *rsa.PrivateKey
		publicKey  *rsa.PublicKey
	)
	if bits == 0 {
		bits = 2048
	}
	privateKey, err := rsa.GenerateKey(rand.Reader, bits)
	publicKey = &privateKey.PublicKey
	return privateKey, publicKey, err
}

func EncodePublicKey(publicKey *rsa.PublicKey) string {
	derPublicKeyBytes := x509.MarshalPKCS1PublicKey(publicKey)
	strPublicKey := base64.StdEncoding.EncodeToString(derPublicKeyBytes)
	return strPublicKey
}

func DecodePublicKey(publicKey string) (bytePublic []byte, err error) {
	bytePublic, err = base64.StdEncoding.DecodeString(publicKey)
	if err != nil {
		return bytePublic, err
	}
	return bytePublic, err
}

func VerifiedSignature(pub *rsa.PublicKey, sumData, signature []byte) bool {
	err := rsa.VerifyPSS(pub, crypto.SHA256, sumData, signature, nil)
	return err == nil
	// verify signature
	// msg := []byte("verifiable message")
	// msgHash := sha256.New()
	// _, err = msgHash.Write(msg)
	// msgHashSum := msgHash.Sum(nil)
	// signature, err := rsa.SignPSS(rand.Reader, privKeyOri, crypto.SHA256, msgHashSum, nil)
	// err = rsa.VerifyPSS(publKeyOri, crypto.SHA256, msgHashSum, signature, nil)
	// If we don't get any error from the `VerifyPSS` method, that means our
	// signature is valid
}

func HashMain() {
	dataMessage := "ducnp learn rsa"
	// prepare key
	privKeyOri, publKeyOri, err := GenerateKeyRSA(2048)
	if err != nil {
		fmt.Println("error generate rsa key")
		return
	}
	strPublKey := EncodePublicKey(publKeyOri)

	// encrypt proces
	bytePublKey, err := DecodePublicKey(strPublKey)
	if err != nil {
		fmt.Println("error decode rsa public key")
		return
	}
	publKey, err := ParserPublicKey(bytePublKey, "DER") // PEM failed
	if err != nil {
		fmt.Println("error parser rsa public key")
		return
	}
	encryptKey, err := rsa.EncryptPKCS1v15(rand.Reader, publKey, []byte(dataMessage))
	if err != nil {
		fmt.Println("error encrypt rsa data")
		return
	}
	base64Info := base64.StdEncoding.EncodeToString(encryptKey)

	// decrypt process
	bytePublic, err := DecodePublicKey(base64Info)
	if err != nil {
		fmt.Println("error decode rsa public key")
		return
	}
	decrytDataByte, err := rsa.DecryptPKCS1v15(rand.Reader, privKeyOri, bytePublic)
	if err != nil {
		fmt.Println("error decrypt data")
		return
	}
	if string(decrytDataByte) != dataMessage {
		fmt.Println("failed")
		return
	}
	fmt.Println("ok")

}

// typePublic: "PEM", "DER", ...
func ParserPublicKey(publicKey []byte, typePublic string) (*rsa.PublicKey, error) {
	var (
		publicKeyRsa *rsa.PublicKey
		err          error
	)
	switch typePublic {
	case "PEM":
		publicKeyDer, err := convertPublicKeyPEMToDER([]byte(publicKey))
		if err != nil {
			return nil, err
		}
		publicKeyRsa, err = x509.ParsePKCS1PublicKey(publicKeyDer)
		if err != nil {
			return nil, err
		}

	case "DER":
		// ParsePKIXPublicKey
		publicKeyRsa, err = x509.ParsePKCS1PublicKey(publicKey)
		if err != nil {
			return nil, err
		}
		// var (
		// 	ok bool
		// )
		// publicKeyRsa, ok = publicKeyCert.(*rsa.PublicKey)
		// if !ok {
		// 	return nil, err
		// }

		//
		// publicKeyCert, err := x509.ParsePKIXPublicKey(publicKey)
		// if err != nil {
		// 	return nil, err
		// }
		// var (
		// 	ok bool
		// )
		// publicKeyRsa, ok = publicKeyCert.(*rsa.PublicKey)
		// if !ok {
		// 	return nil, err
		// }

	default:
		return publicKeyRsa, errors.New("public key type not found")
	}
	return publicKeyRsa, err
}

func ConvertPublicKeyDERToPEM(derPublicKeyBytes []byte) (pemPublicKeyBytes []byte, err error) {
	var (
		rsaPublicKey *rsa.PublicKey
	)
	rsaPublicKey, err = x509.ParsePKCS1PublicKey(derPublicKeyBytes)
	if err != nil {
		return nil, err
	}

	pemPublicKeyBytes, err = x509.MarshalPKIXPublicKey(rsaPublicKey)
	if err != nil {
		return nil, err
	}
	pemPublicKeyBytes = pem.EncodeToMemory(&pem.Block{
		Type:    "PUBLIC KEY",
		Bytes:   pemPublicKeyBytes,
		Headers: map[string]string{},
	})
	return pemPublicKeyBytes, nil
}

func convertPublicKeyPEMToDER(pemPublicKeyBytes []byte) (derPublicKeyBytes []byte, err error) {
	var (
		pub interface{}
	)
	pemBlock, _ := pem.Decode(pemPublicKeyBytes)
	if pemBlock == nil {
		return nil, errors.New("not recorgnized pem block")
	}
	pub, err = x509.ParsePKIXPublicKey(pemBlock.Bytes)
	if err != nil {
		return nil, err
	}

	if rsaPubicKey, ok := pub.(*rsa.PublicKey); ok {
		derPublicKeyBytes = x509.MarshalPKCS1PublicKey(rsaPubicKey)
		return derPublicKeyBytes, nil
	}

	return nil, errors.New("not valid rsa public format")
}
