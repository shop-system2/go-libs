package game

func RunMainGame() {
	// https://github.com/danicat/pacgo
	// RunMainGameStep01()
	RunMainGameStep05()
}

// var (
// 	maze []string
// )

// type sprite struct {
// 	row int
// 	col int
// }

// var ghosts []*sprite
// var player sprite

// func initialise() {
// 	fmt.Println("func initialise")
// 	cbTerm := exec.Command("stty", "cbreak", "-echo")
// 	cbTerm.Stdin = os.Stdin
// 	err := cbTerm.Run()
// 	if err != nil {
// 		log.Fatalln("unable to activate cbreak mode:", err)
// 		return
// 	}
// 	log.Default().Println("end initialise")
// }

// func cleanup() {
// 	fmt.Println("func cleanup")

// 	// cookedTerm := exec.Command("stty", "-cbreak", "echo")
// 	// cookedTerm.Stdin = os.Stdin

// 	// err := cookedTerm.Run()
// 	// if err != nil {
// 	// 	log.Fatalln("unable to activate cooked mode:", err)
// 	// }
// 	// log.Default().Println("end cleanup")

// 	cookedTerm := exec.Command("stty", "-cbreak", "echo")
// 	fmt.Println(cookedTerm)
// 	cookedTerm.Stdin = os.Stdin

// 	err := cookedTerm.Run()
// 	if err != nil {
// 		log.Fatalln("unable to activate cooked mode:", err)
// 	}

// }
// func loadMaze(pathFile string) error {
// 	f, err := os.Open(pathFile)
// 	if err != nil {
// 		return err
// 	}
// 	defer f.Close()

// 	scanner := bufio.NewScanner(f)

// 	for scanner.Scan() {
// 		line := scanner.Text()
// 		maze = append(maze, line)
// 	}
// 	for row, line := range maze {
// 		for col, char := range line {
// 			switch char {
// 			case 'P':
// 				player = sprite{row, col}
// 			case 'G':
// 				ghosts = append(ghosts, &sprite{row, col})

// 			}

// 		}
// 	}
// 	return nil

// }

// func drawDirection() string {
// 	dir := rand.Intn(4)
// 	move := map[int]string{
// 		0: "UP",
// 		1: "DOWN",
// 		2: "RIGHT",
// 		3: "LEFT",
// 	}
// 	return move[dir]
// }
// func moveGhosts() {
// 	for _, g := range ghosts {
// 		dir := drawDirection()
// 		g.row, g.col = makeMove(g.row, g.col, dir)
// 	}
// }

// func printScreen() {
// 	ClearScreen()

// 	for _, line := range maze {
// 		for _, chr := range line {
// 			switch chr {
// 			case '#':
// 				fmt.Printf("%c", chr)
// 			default:
// 				fmt.Print(" ")
// 			}
// 		}
// 		fmt.Println(line)
// 	}

// 	MoveCursor(player.row, player.col)
// 	fmt.Print("P")
// 	for _, g := range ghosts {
// 		MoveCursor(g.row, g.col)
// 		fmt.Print("G")
// 	}

// 	// Move cursor outside of maze drawing area
// 	MoveCursor(len(maze)+1, 0)
// }

// func readInput() (string, error) {
// 	buffer := make([]byte, 100)

// 	cnt, err := os.Stdin.Read(buffer)
// 	if err != nil {
// 		return "", err
// 	}
// 	fmt.Println("cnt", cnt)
// 	if cnt == 1 && buffer[0] == 0x1b {
// 		return "ESC", nil
// 	} else if cnt >= 3 {
// 		if buffer[0] == 0x1b && buffer[1] == '[' {
// 			switch buffer[2] {
// 			case 'A':
// 				fmt.Println("UP")
// 				return "UP", nil
// 			case 'B':
// 				fmt.Println("DOWN")

// 				return "DOWN", nil
// 			case 'C':
// 				fmt.Println("RIGHT")

// 				return "RIGHT", nil
// 			case 'D':
// 				fmt.Println("LEFT")

// 				return "LEFT", nil
// 			}
// 		}
// 	}

// 	return "", nil
// }

// func makeMove(oldRow, oldCol int, dir string) (newRow, newCol int) {
// 	newRow, newCol = oldRow, oldCol

// 	switch dir {
// 	case "UP":
// 		fmt.Println("UP")
// 		newRow = newRow - 1
// 		if newRow < 0 {
// 			newRow = len(maze) - 1
// 		}
// 	case "DOWN":
// 		fmt.Println("DOWN")
// 		newRow = newRow + 1
// 		if newRow == len(maze) {
// 			newRow = 0
// 		}
// 	case "RIGHT":
// 		fmt.Println("RIGHT")

// 		newCol = newCol + 1
// 		if newCol == len(maze[0]) {
// 			newCol = 0
// 		}
// 	case "LEFT":
// 		fmt.Println("LEFT")

// 		newCol = newCol - 1
// 		if newCol < 0 {
// 			newCol = len(maze[0]) - 1
// 		}
// 	}

// 	if maze[newRow][newCol] == '#' {
// 		newRow = oldRow
// 		newCol = oldCol
// 	}

// 	return
// }

// func movePlayer(dir string) {
// 	player.row, player.col = makeMove(player.row, player.col, dir)
// }

// func RunMainGameStep01() {
// 	// initialise game
// 	initialise()
// 	defer cleanup()

// 	err := loadMaze("maze01.txt")
// 	if err != nil {
// 		log.Println("failed to load maze:", err)
// 		return
// 	}
// 	for {
// 		// update screen
// 		printScreen()

// 		// process input
// 		input, err := readInput()
// 		if err != nil {
// 			log.Println("error reading input:", err)
// 			break
// 		}
// 		fmt.Println("input:", input)

// 		// process movement
// 		movePlayer(input)
// 		moveGhosts()
// 		// process collisions

// 		// check game over
// 		if input == "ESC" {
// 			fmt.Println("input = ESC")
// 			break
// 		}

// 		// Temp: break infinite loop
// 		// break

// 		// repeat
// 	}
// }
