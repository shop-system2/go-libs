
from csv import reader, DictReader, writer
from os import listdir
from datetime import date, datetime
import sys
from config import db_exampur
import pandas as pd
from bson.objectid import ObjectId
from mongo import addNewCourse

userFile = "./"+sys.argv[1]

writeFile = userFile.replace("./", "./out_error_").replace(".csv", "")+datetime.now().strftime("%m-%d-%Y-%H:%M:%S")+".csv"
write_obj = open(writeFile, 'w')
csvwriter = writer(write_obj)
csvwriter.writerow(["id", "name", "error"])

course_collection = db_exampur['course']

COURSES = {}

def findCourseId(course_name):
    if (course_name) in COURSES:
        return COURSES[(course_name)]
    else:
        course_count = course_collection.count_documents({"title": course_name})
        if course_count > 0 :
            course = course_collection.find_one({"title": course_name})
            COURSES[(course_name)] = course["_id"]
            return course["_id"]
        else :
            print("COURSE NOT IN DB: ", course_name)
            return ""


def main(csv_path):
    df = pd.read_csv(csv_path)
    # loop row in file
    for i in range(1, df.shape[0]):
        courseId = findCourseId(str(df['name'][i]))
        if courseId == "":
            csvwriter.writerow([df['id'][i], df['name'][i], "NOT FOUND IN EXISTING COURSE"])
            print("NOT FOUND IN COURSE: ", i, "of", df.shape[0], df['name'][i])
            stas, err = addNewCourse(str(df['name'][i]))
            if (not stas):
                csvwriter.writerow([df['id'][i], df['name'][i], err])
            else:
                print("COURSE ADDED SUCCESS FULLY", df['name'][i])
        else:
            print("COURSE ALREADY ADDED: ", df['name'][i])

main(userFile)