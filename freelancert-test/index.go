package freelancerttest

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"strings"
)

// convert python code to golang
func findCourseID(in []string) string {
	f, err := os.Open("store.csv")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		valueLine := strings.Split(scanner.Text(), ",")
		if valueLine[1] == in[1] {
			return valueLine[0]
		}
	}
	return ""
}
func MainTest() {
	fStore, err := os.Create("store1.csv")
	if err != nil {
		log.Fatal(err)
	}
	csvwriter := csv.NewWriter(fStore)

	csvFile, err := os.Open("emp.csv")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println("Successfully Opened CSV file")
	defer csvFile.Close()

	csvLines, err := csv.NewReader(csvFile).ReadAll()
	if err != nil {
		fmt.Println(err)
	}
	for _, line := range csvLines {
		if len(line) != 3 {
			panic("input invalid")
		}
		fmt.Println("file read:", line)
		courseID := findCourseID(line)
		fmt.Println(courseID)
		if courseID != "" {
			fmt.Println("COURSE ALREADY ADDED:", line[1])
			continue
		}
		fmt.Println("add line:", line)
		err = csvwriter.Write([]string{line[0], line[1], line[2]})
		if err != nil {
			log.Fatal(err)
		}
	}
	csvwriter.Flush()
}
