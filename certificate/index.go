package certificate

import (
	"crypto/tls"
	"fmt"
	"time"
)

func RunMain() {
	fmt.Println("ssl")
	hostPort := "blog.umesh.wtf:443"
	host := "blog.umesh.wtf"
	// check wedsite has an ssl certificate
	conn, err := tls.Dial("tcp", hostPort, nil)

	if err != nil {
		fmt.Println(err)
	}
	// check whether the ssl certificate and website hostname match
	err = conn.VerifyHostname(host)
	if err != nil {
		fmt.Println(err)
	}
	// verify expire date of the server is ssl certificate
	expiry := conn.ConnectionState().PeerCertificates[0].NotAfter

	fmt.Printf("Issuer: %s\nExpiry: %v\n", conn.ConnectionState().PeerCertificates[0].Issuer, expiry.Format(time.RFC850))

}
