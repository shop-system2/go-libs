package worker

import (
	"fmt"
	"os"
	"os/signal"
	"time"

	"go.uber.org/zap"
)

func picking() int {
	return 1
}
func adfsf() {
	for tick := range time.Tick(time.Millisecond * 100) {
		fmt.Println(tick)
		change <- picking()
	}
}

func print(value int) {
	fmt.Println("sleep print 2 second")
	time.Sleep(time.Second * 10)
	fmt.Println("print value:", value)
}

var change = make(chan int)

func MainTool() {
	go adfsf()

	for i := 0; i < 10; i++ {
		go func(worerID int) {
			for {
				var adsfsdf = <-change
				fmt.Println("adsfsd", adsfsdf)
				fmt.Println("worerID", worerID)
				print(adsfsdf)
			}
		}(i)
	}

	sig := make(chan os.Signal)
	signal.Notify(sig, os.Interrupt, os.Kill)
	zap.S().Info("worker waiting!!")
	<-sig
}
