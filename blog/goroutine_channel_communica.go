package blog

import (
	"fmt"
	"log"
	"net/http"
	"sync"
	"time"
)

func Main2() {
	fmt.Println("main process 2, sử dung goroutine")
	startTime := time.Now()
	wg := sync.WaitGroup{}
	ch := make(chan string)
	wg.Add(2)
	go process21(&wg, ch)
	go process22(&wg, ch)
	wg.Wait()
	endTime := time.Since(startTime).Milliseconds()
	fmt.Println("tong process-2:", endTime)
}

func process21(wg *sync.WaitGroup, ch chan string) {
	defer wg.Done()
	startTime := time.Now()
	// ví dụ process 1 này có 3 logic, và 1 logic sẽ tốn 100 millisecond
	// 1. là get thông tin user từ database
	fmt.Println("process21: get info user") // sau này có thể thay login ở dòng này
	time.Sleep(time.Millisecond * 100)
	// 2. sau khi có thông tin user thì lấy history các bình luận thông qua userID
	fmt.Println("process21: get bình luận của user")
	time.Sleep(time.Millisecond * 100)
	ch <- "123123"
	// 3. Process này là xem các bình luận nào thuộc bài viết đã được xoá thì cũng xoá bình luận tương ứng đó.
	fmt.Println("process21: xoá bình luận của bình viết không tồn tại")
	time.Sleep(time.Millisecond * 100)
	// => tổn cộng sau khi handle xong thì bạn tốn khoảng hơn 300 millisecond
	endTime := time.Since(startTime).Milliseconds()
	fmt.Println("time process-21:", endTime)
}

func process22(wg *sync.WaitGroup, ch chan string) {
	defer wg.Done()
	startTime := time.Now()
	// ví dụ process 2 tương tự như process 1 cũng có 3 logic, và 1 logic sẽ tốn 100 millisecond
	// 1. là get thông tin user từ database
	fmt.Println("process-22: get info user") // sau này có thể thay login ở dòng này
	time.Sleep(time.Millisecond * 100)
	fmt.Println("process-22: blocking process")
	userID := <-ch
	fmt.Println("process-22:userID", userID)

	// 2. sau khi có thông tin user thì lấy history các bình luận thông qua userID
	fmt.Println("process-22: get bình luận của user")
	time.Sleep(time.Millisecond * 100)
	// 3. Process này là xem các bình luận nào thuộc bài viết đã được xoá thì cũng xoá bình luận tương ứng đó.
	fmt.Println("process-22: xoá bình luận của bình viết không tồn tại")
	time.Sleep(time.Millisecond * 100)
	// => tổn cộng sau khi handle xong thì bạn tốn khoảng hơn 300 millisecond
	endTime := time.Since(startTime).Milliseconds()
	fmt.Println("time process-22:", endTime)
}

func Main1() {
	fmt.Println("main process 1 khong dung goroutine")
	startTime := time.Now()
	process1()
	process2()
	endTime := time.Since(startTime).Milliseconds()
	fmt.Println("tong process:", endTime)
}

func process1() {
	startTime := time.Now()
	// ví dụ process 1 này có 3 logic, và 1 logic sẽ tốn 100 millisecond
	// 1. là get thông tin user từ database
	fmt.Println("process1: get info user") // sau này có thể thay login ở dòng này
	time.Sleep(time.Millisecond * 100)
	// 2. sau khi có thông tin user thì lấy history các bình luận thông qua userID
	fmt.Println("process1: get bình luận của user")
	time.Sleep(time.Millisecond * 100)
	// 3. Process này là xem các bình luận nào thuộc bài viết đã được xoá thì cũng xoá bình luận tương ứng đó.
	fmt.Println("process1: xoá bình luận của bình viết không tồn tại")
	time.Sleep(time.Millisecond * 100)
	// => tổn cộng sau khi handle xong thì bạn tốn khoảng hơn 300 millisecond
	endTime := time.Since(startTime).Milliseconds()
	fmt.Println("time process 1:", endTime)
}

func process2() {
	startTime := time.Now()
	// ví dụ process 1 này có 3 logic, và 1 logic sẽ tốn 100 millisecond
	// 1. Get thông tin tất cả các bài viết
	fmt.Println("process2: get thông tin bài viết") // sau này có thể thay login ở dòng này
	time.Sleep(time.Millisecond * 100)
	// 2. Thực hiện fillter các bài viết
	fmt.Println("process2: thực hiện fillter trên code")
	time.Sleep(time.Millisecond * 100)
	// 3. Process này là sort data.
	fmt.Println("process2: xử lý sort data để trả về tăng dần")
	time.Sleep(time.Millisecond * 100)
	// => tổn cộng sau khi handle xong thì bạn tốn khoảng hơn 300 millisecond
	endTime := time.Since(startTime).Milliseconds()
	fmt.Println("time process 2:", endTime)
}

func githubStatus(i int, ch chan string) (string, error) {
	log.Println("call githubStatus with i=", i)
	resp, err := http.Get("https://api.github.com")
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("github response: %s", resp.Status)
	}
	fmt.Println("call ok, github-status", resp.Status)
	ich := <-ch
	if ich != "" {
		fmt.Println("other api with signal", ich)
	}
	return resp.Status, err
}
