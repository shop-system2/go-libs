package blog

import (
	"fmt"
	"sync"
	"sync/atomic"
)

func Main() {
	var i int32
	var wg sync.WaitGroup
	wg.Add(3)
	go Process(&i, &wg)
	go Process(&i, &wg)
	go Process(&i, &wg)
	wg.Wait()
	fmt.Println("i:", i)
}

func Process(variable *int32, wg *sync.WaitGroup) {
	defer wg.Done()
	for i := 0; i < 2000; i++ {
		atomic.AddInt32(variable, 1)
	}
}
