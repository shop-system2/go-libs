package blog

import (
	"fmt"
	"time"
)

func exChannel(ch chan int) {
	sendvalue := 5
	ch <- sendvalue
}
func ag(a chan int) {
	fmt.Println(<-a)
	// fmt.Println(<-a)
	// fmt.Println(<-a)
}

func MainChannel() {
	stop := make(chan bool)
	ch := make(chan int)
	go exChannelArray(ch, stop)
	for {
		select {
		case receice := <-ch:
			fmt.Println("receice:", receice)
		case <-stop:
			return
		}
	}
}

func exChannelArray(ch chan int, stop chan bool) {
	for i := 0; i < 9; i++ {
		ch <- i
	}
	time.Sleep(time.Second)
	stop <- true
}
