package blog

import (
	"context"
	"errors"
	"fmt"
	"time"
)

func main1() {

	taskfn := func(ctx context.Context) <-chan int {
		dst := make(chan int)
		n := 1
		go func() {
			for {
				select {
				case <-ctx.Done():
					fmt.Println("exe cannel")
					return // returning not to leak the goroutine
				case dst <- n:
					n++
				}
			}
		}()
		return dst
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel() // cancel when we are finished consuming integers
	for n := range taskfn(ctx) {
		fmt.Println(n)
		if n == 5 {
			break
		}
	}

}

func func1(ctx context.Context) error {
	time.Sleep(100 * time.Millisecond)
	return errors.New("failed")
}

func func2(ctx context.Context) {
	fmt.Println("exe operation2")
	select {
	case <-time.After(50 * time.Millisecond):
		fmt.Println("done")
	case <-ctx.Done():
		fmt.Println("halted operation2")
	}
}

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	go func() {
		err := func1(ctx)
		fmt.Println("operation1 err:", err)
		if err != nil {
			cancel()
		}
	}()

	func2(ctx)
}
