package kafka

import (
	"context"
	"fmt"
	"log"
	"os"
	"strconv"
	"time"

	"github.com/segmentio/kafka-go"
	"github.com/segmentio/kafka-go/sasl/scram"
)

func producer() {
	conReq := KafkaRequest{
		Async:    true,
		Topics:   []string{"logging"},
		Broker:   []string{"localhost:9092"},
		Version:  "",
		Username: "",
		Password: "",
		Group:    "test-consumer-group",
	}
	pro, err := NewKafkaProducer(conReq)
	if err != nil {
		panic(err)
	}
	i := 0
	for range time.Tick(time.Second) {
		i++
		// Prints UTC time and date
		// fmt.Println(tick)
		err = pro.Send(context.Background(), KafkaPropertySend{
			ProducerName: "a",
			Topic:        "logging",
			Key:          "KWy",
		}, LoggingDTO{})
		if err != nil {
			fmt.Println("send err", err)
		}
		if i > 10 {
			break
		}
	}
}
func consumer() {
	kafka := NewKafka()
	kafkaReq := KafkaRequest{
		Async:    true,
		Topics:   []string{"logging"},
		Broker:   []string{"localhost:9092"},
		Version:  "",
		Username: "",
		Password: "",
		Group:    "test-consumer-group",
	}
	err := kafka.InitKafka(kafkaReq)
	if err != nil {
		panic(err)
	}
}
func MainKafka() {
	forever := make(chan bool)
	fmt.Println("main kafka")
	go consumer()
	// go producer()
	<-forever
}

const (
	topic         = "topic"
	topic1        = "topic1"
	topic2        = "topic2"
	brokerAddress = "localhost:9092"
	group         = "test-consumer-group"
)

func MainKafka2() {
	forever := make(chan bool)
	// create a new context
	ctx := context.Background()
	// produce messages in a new go routine, since
	// both the produce and consume functions are
	// blocking
	//go produce(ctx)

	// go consume(ctx, topic1, 0)
	// go consume(ctx, topic1, 1)
	// go consume(ctx, topic1, 2)
	go consume(ctx, topic2, 0)
	<-forever
}

func produce(ctx context.Context) {
	// initialize a counter
	i := 0

	l := log.New(os.Stdout, "kafka writer: ", 0)
	// intialize the writer with the broker addresses, and the topic
	w := kafka.NewWriter(kafka.WriterConfig{
		Brokers: []string{brokerAddress},
		Topic:   topic,
		// assign the logger to the writer
		Logger: l,
		Async:  false,
	})

	for {
		// each kafka message has a key and value. The key is used
		// to decide which partition (and consequently, which broker)
		// the message gets published on
		err := w.WriteMessages(ctx, kafka.Message{
			Key: []byte(strconv.Itoa(i)),
			// create an arbitrary message payload for the value
			Value: []byte("this is message" + strconv.Itoa(i)),
		})
		if err != nil {
			panic("could not write message " + err.Error())
		}

		// log a confirmation once the message is written
		fmt.Println("writes:", i)
		i++
		// sleep for a second
		time.Sleep(time.Second)
	}
}

func consume(ctx context.Context, topic string, pa int) {
	// create a new logger that outputs to stdout
	// and has the `kafka reader` prefix
	l := log.New(os.Stdout, "kafka reader: ", 0)
	// initialize a new reader with the brokers and topic
	// the groupID identifies the consumer and prevents
	// it from receiving duplicate messages
	mechanism, err := scram.Mechanism(scram.SHA512, "username", "password")
	if err != nil {
		panic(err)
	}

	dialer := &kafka.Dialer{
		Timeout:       10 * time.Second,
		DualStack:     true,
		SASLMechanism: mechanism,
	}

	r := kafka.NewReader(kafka.ReaderConfig{
		Brokers: []string{brokerAddress},
		Topic:   topic,
		GroupID: group,
		// assign the logger to the reader
		Logger: l,
		Dialer: dialer,
		// Partition: pa,
		// GroupTopics: []string{},
		// MinBytes: 10e3, // 10KB
		// MaxBytes: 10e6, // 10MB
	})
	for {

		m, err := r.FetchMessage(ctx)

		if err != nil {
			break
		}
		fmt.Printf("message at topic/partition/offset %v/%v/%v: %s = %s\n", m.Topic, m.Partition, m.Offset, string(m.Key), string(m.Value))
		if err := r.CommitMessages(ctx, m); err != nil {
			log.Fatal("failed to commit messages:", err)
		}
	}

	for {
		// the `ReadMessage` method blocks until we receive the next event
		msg, err := r.ReadMessage(ctx)
		// now := time.Now()
		// time.Sleep(time.Second * 3)
		// fmt.Println(time.Since(now).Milliseconds())
		if err != nil {
			panic("could not read message " + err.Error())
		}
		// after receiving the message, log its value
		fmt.Println("received: ", string(msg.Value))
	}
}
