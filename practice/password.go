package practice

import (
	"bytes"
	"encoding/hex"
	"fmt"
	"strings"
)

func DecryptPass(encrypt string, key string) (string, error) {
	var (
		decryptPass string
	)
	deKey := int(hashCode(key))
	fmt.Println(deKey)
	bytelist, err := hex.DecodeString(encrypt)
	if err != nil {
		return decryptPass, err
	}
	for i := 0; i < len(bytelist); i++ {
		bytelist[i] = bytelist[i] ^ byte(deKey)
	}
	str := string(bytelist)
	fmt.Println(str)
	buf := make([]bytes.Buffer, len(bytelist))

	// json.HTMLEscape(&buf, bytelist)
	// fmt.Println(buf.String())
	for i := 0; i < len(bytelist); i++ {
		// ele := bytelist[i]
		buf = append(buf)
	}
	return decryptPass, err
}

func hashCode(s string) uint32 {
	var h int32 = 0
	for _, r := range s {
		h = 31*h + r
	}
	return uint32(h)
}

func hex2(text string) []byte {
	var (
		bytext []byte
	)
	len := len(text)
	chars := strings.Split(text, "")
	fmt.Println(len, chars)
	for i := 0; i < len/2; i++ {
		bytext = append(bytext, castChar(chars[2*i]))
	}
	return bytext
}

func charText(char string) byte {
	var by byte

	return by
}

func castChar(char string) (by byte) {
	by = []byte(char)[0]
	if ("a" <= char) && (char <= "f") {
		return by - 97 + 10
	}

	fsd := by ^ 1
	fmt.Println(fsd)
	if 0 <= by && by <= 9 {
		return by - 48
	}

	if ("A" <= char) && (char <= "F") {
		return by - 65 + 10
	}
	return by
}
