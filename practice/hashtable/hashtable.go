package hashtable

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sync"

	"github.com/cheekybits/genny/generic"
)

func WriteDataFile() {
	f, err := os.Create("data.txt")

	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	for i := 0; i < 10000000; i++ {
		value := "KV" + fmt.Sprintf("%s%d%s", "0335280715", i, "\n")
		data := []byte(value)

		_, err2 := f.Write(data)

		if err2 != nil {
			log.Fatal(err2)
		}

	}

	// val2 := "0335280715\n"
	// data2 := []byte(val2)

	// var idx int64 = int64(len(data))

	// _, err3 := f.WriteAt(data, idx)

	// if err3 != nil {
	// log.Fatal(err3)
	// }

	fmt.Println("done")
}

func MainHashtable() {
	WriteDataFile()

	f, err := os.Open("data.txt")
	if err != nil {
		log.Fatal(err)
	}
	// remember to close the file at the end of the program
	defer f.Close()

	// read the file line by line using scanner
	scanner := bufio.NewScanner(f)
	testMap := make(map[int]bool)
	i := 0
	for scanner.Scan() {
		// do something with a line
		// fmt.Printf("line: %s\n", scanner.Text())
		valu := scanner.Text()

		hashVa := hash(valu)
		_, ok := testMap[hashVa]
		if ok {
			fmt.Println("existys")
		}

		if i%100000 == 0 {
			fmt.Println("sleep", i)
		}
		testMap[hashVa] = true
		i++
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	fmt.Println(hash("KV0335280715"))
	fmt.Println(hash("HCVN0335280715"))
	fmt.Println(hash("ATOME0335280715"))
	TestPut()
	TestRemove()
}

type Key generic.Type
type Value generic.Type

type ValueHashtable struct {
	items map[int]Value
	lock  sync.RWMutex
}

func hash(k Key) int {
	key := fmt.Sprintf("%s", k)
	h := 0
	// fmt.Println(len(key))
	for i := 0; i < len(key); i++ {
		// fmt.Println(int(key[i]))
		h = 31*h + int(key[i])
		// fmt.Println(h)
	}
	return h
}

func (ht *ValueHashtable) Put(k Key, value Value) {
	ht.lock.Lock()
	defer ht.lock.Unlock()
	j := hash(k)
	if ht.items == nil {
		ht.items = make(map[int]Value)
	}
	ht.items[j] = value
}

func (ht *ValueHashtable) Get(k Key) Value {
	ht.lock.RLock()
	defer ht.lock.RUnlock()
	i := hash(k)
	return ht.items[i]
}

// Size returns the number of the hashtable elements
func (ht *ValueHashtable) Size() int {
	ht.lock.RLock()
	defer ht.lock.RUnlock()
	return len(ht.items)
}
func (ht *ValueHashtable) Remove(k Key) {
	ht.lock.Lock()
	defer ht.lock.Unlock()
	j := hash(k)
	delete(ht.items, j)
}

func populateHashtable(count int, start int) *ValueHashtable {
	dict := ValueHashtable{}
	for i := start; i < (start + count); i++ {
		dict.Put(fmt.Sprintf("key%d", i), fmt.Sprintf("value%d", i))
	}

	return &dict
}

// func TestPut(t *testing.T) {
func TestPut() {
	dict := populateHashtable(3, 0)
	if size := dict.Size(); size != 3 {

		fmt.Println("wrong count, expected 3 and got ", size)
	}
	dict.Put("key1", "value1") //should not add a new one, just change the existing one
	if size := dict.Size(); size != 3 {

		fmt.Println("wrong count, expected 3 and got ", size)
	}
	dict.Put("key4", "value4") //should add it
	if size := dict.Size(); size != 4 {

		fmt.Println("wrong count, expected 4 and got ", size)
	}
}

func TestRemove() {
	dict := populateHashtable(3, 0)
	dict.Remove("key2")
	if size := dict.Size(); size != 2 {

		fmt.Println("wrong count, expected 2 and got ", size)
	}
}
