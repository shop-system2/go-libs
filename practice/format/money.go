package format

import (
	"strconv"
	"strings"
)

func NumberFormat(val float64, precision int) string {
	var thousandSep = byte('.')
	var decSep = byte(',')
	// Parse the float as a string, with no exponent, and keeping precision
	// number of decimal places. Note that the precision passed in to FormatFloat
	// must be a positive number.
	usePrecision := precision
	if precision < 1 {
		usePrecision = 1
	}
	asString := strconv.FormatFloat(val, 'f', usePrecision, 64)
	// Split the string at the decimal point separator.
	separated := strings.Split(asString, ".")
	beforeDecimal := separated[0]
	// Our final string will need a total space of the original parsed string
	// plus space for an additional separator character every 3rd character
	// before the decimal point.
	withSeparator := make([]byte, 0, len(asString)+(len(beforeDecimal)/3))

	// Deal with a (possible) negative sign:
	if beforeDecimal[0] == '-' {
		withSeparator = append(withSeparator, '-')
		beforeDecimal = beforeDecimal[1:]
	}

	// Drain the initial characters that are "left over" after dividing the length
	// by 3. For example, if we had "12345", this would drain "12" from the string
	// append the separator character, and ensure we're left with something
	// that is exactly divisible by 3.
	initial := len(beforeDecimal) % 3
	if initial > 0 {
		withSeparator = append(withSeparator, beforeDecimal[0:initial]...)
		beforeDecimal = beforeDecimal[initial:]
		if len(beforeDecimal) >= 3 {
			withSeparator = append(withSeparator, thousandSep)
		}
	}

	// For each chunk of 3, append it and add a thousands separator,
	// slicing off the chunks of 3 as we go.
	for len(beforeDecimal) >= 3 {
		withSeparator = append(withSeparator, beforeDecimal[0:3]...)
		beforeDecimal = beforeDecimal[3:]
		if len(beforeDecimal) >= 3 {
			withSeparator = append(withSeparator, thousandSep)
		}
	}
	// Append everything after the '.', but only if we have positive precision.
	if precision > 0 {
		withSeparator = append(withSeparator, decSep)
		withSeparator = append(withSeparator, separated[1]...)
	}
	return string(withSeparator)
}
