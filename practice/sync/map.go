package sync

import (
	"fmt"
	"sync"
	"sync/atomic"
	"time"
)

func RunSyncMap() {
	fmt.Println("sync map")
	tempMap := Counter{sync.Map{}}
	add := tempMap.Add("ss", 2)
	fmt.Println(add)
	time.Sleep(time.Millisecond * 100)

	demo, ok := tempMap.Get("ss")
	fmt.Println(demo, ok)
}

// why are the need using LoadAndDelete and LoadOrStore
type Counter struct {
	m sync.Map
}

// get retriever the count without modifying it
func (c *Counter) Get(key string) (int64, bool) {
	count, ok := c.m.Load(key)
	if ok {
		return atomic.LoadInt64(count.(*int64)), true
	}
	return 0, false
}

//
func (c *Counter) AddUnsafe(key string, value int64) int64 {
	count, loaded := c.m.Load(key)
	if loaded {
		c.m.Store(key, &value)
		return 0
	}
	return *count.(*int64)
}

// using sync map
func (c *Counter) Add(key string, value int64) int64 {
	count, loaded := c.m.LoadOrStore(key, &value)
	if loaded {
		return atomic.AddInt64(count.(*int64), value)
	}
	return *count.(*int64)
}

func (c *Counter) DeleteAndGetLastValue(key string) (int64, bool) {
	lastValue, loaded := c.m.LoadAndDelete(key)
	if loaded {
		return *lastValue.(*int64), loaded
	}
	return 0, false
}

/* *****
demo 2
***** */

type RWMutexMap struct {
	mu    sync.RWMutex
	dirty map[interface{}]interface{}
}

func (m *RWMutexMap) Load(key interface{}) (value interface{}, ok bool) {
	m.mu.Lock()
	value, ok = m.dirty[key]
	m.mu.Unlock()
	return
}

func (m *RWMutexMap) Store(key, value interface{}) {
	m.mu.Lock()
	if m.dirty == nil {
		m.dirty = make(map[interface{}]interface{})
	}
	m.dirty[key] = value
	m.mu.Unlock()
}

func (m *RWMutexMap) LoadOrStore(key, value interface{}) (actual interface{}, loaded bool) {
	m.mu.Lock()
	actual, loaded = m.dirty[key]
	if !loaded {
		actual = value
		if m.dirty == nil {
			m.dirty = make(map[interface{}]interface{})
		}
		m.dirty[key] = value
	}
	m.mu.Unlock()
	return actual, loaded
}

func (m *RWMutexMap) LoadAndDelete(key interface{}) (value interface{}, loaded bool) {
	m.mu.Lock()
	value, loaded = m.dirty[key]
	if !loaded {
		m.mu.Unlock()
		return nil, false
	}
	delete(m.dirty, key)
	m.mu.Unlock()
	return value, loaded
}
func (m *RWMutexMap) Delete(key interface{}) {
	m.mu.Lock()
	delete(m.dirty, key)
	m.mu.Unlock()
}

func (m *RWMutexMap) Range(f func(key, value interface{}) (shouldContinue bool)) {
	m.mu.RLock()
	keys := make([]interface{}, 0, len(m.dirty))
	for k := range m.dirty {
		keys = append(keys, k)
	}
	m.mu.RUnlock()

	for _, k := range keys {
		v, ok := m.Load(k)
		if !ok {
			continue
		}
		if !f(k, v) {
			break
		}
	}
}
