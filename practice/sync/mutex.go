package sync

import (
	"fmt"
	"sync"
	"time"
)

type MutexCounter struct {
	mu    sync.Mutex
	value map[string]int
}

func (m *MutexCounter) Increment(key string, i int) {
	// fmt.Println("add", i)
	m.mu.Lock()
	m.value[key]++
	m.mu.Unlock()
}

func (m *MutexCounter) ValueKey(key string) int {
	return m.value[key]
}

func DemoMutex1() {
	fmt.Println("run mutext demo 1")
	c := MutexCounter{value: make(map[string]int)}
	for i := 0; i < 10; i++ {
		go c.Increment("demo", i)
		go c.Increment("demo", i)
		go c.Increment("demo", i)
		go c.Increment("demo", i)
		go c.Increment("demo", i)
	}
	time.Sleep(time.Second)
	fmt.Println(c.ValueKey("demo"))
}

func RunMutext() {
	DemoMutex1()
	DemoMutex2()
}

func DemoMutex2() {

}
