package sync

import "testing"

// from fib_test.go
func BenchmarkFib10(b *testing.B) {
	// run the Fib function b.N times
	for n := 0; n < b.N; n++ {
		RunMutext()
	}
}

//  go test -bench=BenchmarkFib10
func BenchmarkReduceAllocation(b *testing.B) {
	// run the Fib function b.N times

	for n := 0; n < b.N; n++ {
		RunAtomic()
	}
	b.ReportAllocs()

}

// go test -bench=BenchmarkReduceAllocation
// go test -bench=BenchmarkReduceAllocation > a.txt // save output to file
// 235           5126292 ns/op              50 B/op          3 allocs/op
// PASS
// ok      go-libs/practice/sync   2.819s
