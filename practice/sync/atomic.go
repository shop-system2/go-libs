package sync

import (
	"fmt"
	"sync"
)

func RunAtomic() {
	// fmt.Println("run atomic")
	var ops uint64
	var wg sync.WaitGroup

	for i := 0; i < 1000; i++ {
		wg.Add(1)
		go func() {
			for c := 0; c < 100; c++ {
				// atomic.AddUint64(&ops, 1)
				// atomic.AddUint64(&ops, 1)
				ops++
			}
			wg.Done()
		}()
	}
	wg.Wait()
	fmt.Println("ops:", ops)
	// tempOps := atomic.LoadUint64(&ops)
	// fmt.Println("ops1:", tempOps)

	// atomic.StoreUint64(&ops, 1)

	// time.Sleep(100 * time.Millisecond)

	// tempOps1 := atomic.LoadUint64(&ops)
	// fmt.Println("ops2:", tempOps1)
}
