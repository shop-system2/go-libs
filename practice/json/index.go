package json

import (
	"encoding/json"
	"fmt"
)

type Datatest struct {
	Name     string `json:"name"`
	Password string `json:"password"`
	Phone    string `json:"phone"`
	Address  string `json:"address"`
	Numbers  int    `json:"numbers"`
}

func MainRun() {
	data := Datatest{
		Name:     "duc",
		Password: "123",
	}
	databyte, err := json.Marshal(data)

	fmt.Println(databyte, err)

	new := Datatest{}
	err = json.Unmarshal(databyte, &new)
	fmt.Println(err)
}
