package practice

import (
	"fmt"
	"os"
	"runtime/trace"
	"unsafe"
)

type Infomation struct {
	ValueUint32 uint32 // 4 byte
	ValueBool   bool   // 1 byte
	ValueUint64 uint64 // 8 byte
	ValueBool2  bool   // 1 byte
}

type Infomation2 struct {
	ValueUint64 uint64 // 8 byte
	ValueUint32 uint32 // 4 byte
	ValueBool   bool   // 1 byte
	ValueBool2  bool   // 1 byte
}

func TraceGolang() {

	trace.Start(os.Stderr)
	defer trace.Stop()
	item := Infomation{}
	item2 := Infomation2{}
	fmt.Println("tong 1: ", unsafe.Sizeof(item))
	fmt.Println("tong 2: ", unsafe.Sizeof(item2))
	// create new channel of type int
	ch := make(chan int)

	// start new anonymous goroutine
	go func() {
		// send 42 to channel
		ch <- 42
	}()
	// read from channel
	<-ch
}

// go run main.go 2> trace.out
// go tool trace trace.out

func ValueCopyCost() {

}
