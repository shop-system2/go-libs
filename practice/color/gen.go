package color

import (
	"fmt"
	"math"
)

func Rainbow(numOfSteps, step float64) (int, int, int) {
	var r, g, b float64
	h := step / numOfSteps
	i := math.Floor(h * 6)
	f := h*6 - i
	q := 1 - f
	os := math.Remainder(i, 6)
	fmt.Println(os, h, i, f, q)
	switch os {
	case 0:
		r = 1
		g = f
		b = 0
	case 1:
		r = q
		g = 1
		b = 0
	case 2:
		r = 0
		g = 1
		b = f
	case 3:
		r = 0
		g = q
		b = 1
	case 4:
		r = f
		g = 0
		b = 1
	case 5:
		r = 1
		g = 0
		b = q
	}
	r = r * 255
	g = g * 255
	b = b * 255
	return int(r), int(g), int(b)
}
