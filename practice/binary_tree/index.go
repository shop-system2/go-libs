package binarytree

import "fmt"

type RightSideTreeNode struct {
	Val   int
	Left  *RightSideTreeNode
	Right *RightSideTreeNode
}

func RightSideView() []int {
	root := &RightSideTreeNode{
		Val: 1,
		Left: &RightSideTreeNode{
			Val:  2,
			Left: &RightSideTreeNode{},
		},
	}
	fmt.Println(root.Val, root.Left.Right, root.Left)
	var result []int
	return result
}
