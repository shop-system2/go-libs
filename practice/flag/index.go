package flag

import (
	"flag"
	"fmt"
	"time"
)

func MainFlag() {
	var strFile string
	flag.StringVar(&strFile, "1", "2", "3")
	flag.Parse()
	fmt.Println(strFile)
	time.Sleep(time.Second * 10)

}
