
create database test;
use test;
create table employee (
    idd int NOT NULL,
    salary VARCHAR(255) NULL
);
select * from employee;
select count(*) from employee;
delete from employee where idd >= 1;

insert into employee (idd, salary) values (1, "100");
insert into employee (idd, salary) values (2, "200");
insert into employee (idd, salary) values (3, "300");
select 
    (
    if( count(*) <> 1, max(salary), NULL)
  --  else if (count(*) = 1, min(salary), NULL ) 
    )
    as SecondHighestSalary from (
    select salary from employee order by salary desc limit 1 offset 0
) as s;
select *,dense_rank() over (order by salary desc) as numRow from employee;


select 
    (
    if( count(*) < 2, max(salary), 
            (
select salary from employee
    order by salary desc limit 1 offset 1
            ) 
     )
    )
    as SecondHighestSalary from (
   select salary from employee
) as s;




select 
if (count(*) < 2, max(salary), NULL) as test1
from employee;

select idd,
case 
    when count(*) < 2 then (select max(salary) from employee) as aa
end (select salary from employee order by salary desc limit 1 offset 1) as aa
from employee;
select max(salary)  from (
    select *, dense_rank() over (order by salary desc) as numRow from employee
) as tableTemp
where tableTemp.numRow = 2;

 select (if ( count( select dense_rank() over (order by salary desc) as num from employee) < 2, max(salary), NULL)) as SecondHighestSalary
 from (
    select salary from employee order by salary desc limit 1 offset 1
) as s;
