package array

import "fmt"

func ChunkSliceImage(sliceImage []int, chunkSize int) [][]int {
	var chunks [][]int
	for i := 0; i < len(sliceImage); i += chunkSize {
		end := i + chunkSize

		// necessary check to avoid slicing beyond
		// slice capacity
		if end > len(sliceImage) {
			end = len(sliceImage)
		}
		chunks = append(chunks, sliceImage[i:end])
	}
	return chunks
}

func splitI(list []int, thread int) {
	if thread == 0 {
		thread = 1
	}
	lenLi := len(list)
	count := lenLi / thread
	var newList [][]int
	for i := 0; i < thread; i++ {
		start := i * count
		end := start + count
		if end > lenLi {
			end = lenLi
		}
		if i == (thread - 1) {
			newList = append(newList, list[start:lenLi])
		} else {
			newList = append(newList, list[start:end])
		}
	}
	if len(newList) == 0 {
		newList = append(newList, list)
	}
	fmt.Println(newList)
}
