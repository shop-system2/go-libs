package array

import "fmt"

func paren(tem string, left, right int) (item string) {
	if left == 0 && right == 0 {
		return tem
	}
	if left > 0 {
		left = left - 1
		tem = paren(tem+"(", left, right)
	}
	if right > 0 && right > left {
		right = right - 1
		tem = paren(tem+")", left, right)
	}
	return tem
}

func GenerateParentheses(n int) (result []string) {
	item := paren("", n, n)
	result = append(result, item)
	fmt.Println(result)
	return result
}
