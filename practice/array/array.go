package array

// ArraySum hackerrank test pass
func ArraySum([]int32) int32 {
	arr := []int32{1, 2, 3, 4, 5}
	sum := int32(0)
	for i := 0; i < len(arr); i++ {
		sum += arr[i]
	}
	return sum
}

// ComparaTriplets hackerrank test pass
func CompareTriplets(a []int32, b []int32) []int32 {
	var (
		pointA int32
		pointB int32
		j      int32
		result []int32
	)
	for i := 0; i < len(a); i++ {
		j = int32(i)
		if a[j] < b[j] {
			pointB += 1
		}
		if a[j] > b[j] {
			pointA += 1
		}
	}
	result = append(result, pointA, pointB)
	return result
}

// AVeryBigSum hackerrank test pass
func AVeryBigSum(ar []int64) int64 {
	var sum int64 = 0
	for i := 0; i < len(ar); i++ {
		sum += ar[i]
	}
	return sum
}
