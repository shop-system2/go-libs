package array

import (
	"fmt"
)

var (
	appreas = map[string]string{
		"1": "1",
		"2": "2",
		"3": "3",
		"4": "4",
		"5": "5",
		"6": "6",
		"7": "7",
		"8": "8",
		"9": "9",
	}
)

func checkValidArray(subBoard []string) bool {
	tempMap := map[string]bool{}
	// isApprear := 0
	for i := 0; i < len(subBoard); i++ {
		if subBoard[i] == "." {
			continue
		}
		if _, ok := tempMap[subBoard[i]]; ok {
			return false
		}
		tempMap[subBoard[i]] = true
	}
	return true
}

func isValidSudoku(board [][]string) bool {
	// check row and column
	for i := 0; i < len(board); i++ {
		isValid := checkValidArray(board[i])
		// fmt.Println("row valid:", isValid)
		if !isValid {
			return false
		}
		var columnValud = []string{}
		for j := 0; j < 9; j++ {
			columnValud = append(columnValud, board[j][i])
		}
		isValidColumn := checkValidArray(columnValud)
		// fmt.Println("column valid:", isValidColumn)
		if !isValidColumn {
			return false
		}
	}
	// check sub-boxes
	for i := 0; i < len(board); i = i + 3 {
		for j := 0; j < len(board); j = j + 3 {
			var columnValud = []string{}
			// fmt.Println(i, j, board[i][j])

			for k := 0; k < 3; k++ {
				for l := 0; l < 3; l++ {
					// fmt.Println(i+k, j+l, board[i+k][j+l])
					columnValud = append(columnValud, board[i+k][j+l])
				}
			}
			isValidSubBox := checkValidArray(columnValud)
			if !isValidSubBox {
				return false
			}
			// fmt.Println("sub-box valid:", isValidSubBox)
		}
		// fmt.Println(columnValud)
	}
	return true
}

func checkValidArrayByte(subBoard []byte) bool {
	tempMap := map[byte]bool{}
	// isApprear := 0
	for i := 0; i < len(subBoard); i++ {
		if subBoard[i] == '.' {
			continue
		}
		if _, ok := tempMap[subBoard[i]]; ok {
			return false
		}
		tempMap[subBoard[i]] = true
	}
	return true
}

func isValidSudokuByte(board [][]byte) bool {
	// check row and column
	for i := 0; i < len(board); i++ {
		isValid := checkValidArrayByte(board[i])
		// fmt.Println("row valid:", isValid)
		if !isValid {
			return false
		}
		var columnValud = []byte{}
		for j := 0; j < 9; j++ {
			columnValud = append(columnValud, board[j][i])
		}
		isValidColumn := checkValidArrayByte(columnValud)
		// fmt.Println("column valid:", isValidColumn)
		if !isValidColumn {
			return false
		}
	}
	// check sub-boxes
	for i := 0; i < len(board); i = i + 3 {
		for j := 0; j < len(board); j = j + 3 {
			var columnValud = []byte{}
			// fmt.Println(i, j, board[i][j])

			for k := 0; k < 3; k++ {
				for l := 0; l < 3; l++ {
					// fmt.Println(i+k, j+l, board[i+k][j+l])
					columnValud = append(columnValud, board[i+k][j+l])
				}
			}
			isValidSubBox := checkValidArrayByte(columnValud)
			if !isValidSubBox {
				return false
			}
			// fmt.Println("sub-box valid:", isValidSubBox)
		}
		// fmt.Println(columnValud)
	}
	return true
}

func RunIsValidSudoku() {
	board1 := [][]byte{
		{'8', '3', '.', '.', '7', '.', '.', '.', '.'},
		{'6', '.', '.', '1', '9', '5', '.', '.', '.'},
		{'.', '9', '8', '.', '.', '.', '.', '6', '.'},
		{'8', '.', '.', '.', '6', '.', '.', '.', '3'},
		{'4', '.', '.', '8', '.', '3', '.', '.', '1'},
		{'7', '.', '.', '.', '2', '.', '.', '.', '6'},
		{'.', '6', '.', '.', '.', '.', '2', '8', '.'},
		{'.', '.', '.', '4', '1', '9', '.', '.', '5'},
		{'.', '.', '.', '.', '8', '.', '.', '7', '9'},
	}
	fmt.Println(board1)
	if isValidSudokuByte(board1) {
		fmt.Println("result: true")
		return
	}
	fmt.Println("result: false")

	fmt.Println()
	fmt.Println()

	board2 := [][]string{
		{"5", "3", ".", ".", "7", ".", ".", ".", "."},
		{"6", ".", ".", "1", "9", "5", ".", ".", "."},
		{".", "9", "8", ".", ".", ".", ".", "6", "."},
		{"8", ".", ".", ".", "6", ".", ".", ".", "3"},
		{"4", ".", ".", "8", ".", "3", ".", ".", "1"},
		{"7", ".", ".", ".", "2", ".", ".", ".", "6"},
		{".", "6", ".", ".", ".", ".", "2", "8", "."},
		{".", ".", ".", "4", "1", "9", ".", ".", "5"},
		{".", ".", ".", ".", "8", ".", ".", "7", "9"},
	}
	fmt.Println(board2)

	if isValidSudoku(board2) {
		fmt.Println("result: true")
		return
	}
	fmt.Println("result: false")

}
