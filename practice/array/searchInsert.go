package array

import "fmt"

func searchInsert(nums []int, target int) (value int) {
	pos := 0
	for i := 0; i < len(nums); i++ {
		if nums[i] == target {
			return i
		}
		if target > nums[i] {
			pos = i + 1
		}
	}
	return pos
}

func Run() {
	var nums = []int{1, 3, 5, 6}
	var target = 0
	value := searchInsert(nums, target)
	fmt.Println(value)
}
