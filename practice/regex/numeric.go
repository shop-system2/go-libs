package regexcheck

import (
	"encoding/json"
	"fmt"
	"reflect"
	"regexp"
	"strings"
	"time"
)

func CheckNumeric(input string) bool {
	matched, err := regexp.MatchString("^\\d+$", input)
	fmt.Println("CheckNumeric err:", err)
	return matched
}

func findPattern() {
	pattern := `DUNCP\sTHM\sNO\s(?P<SuffixCardNumber>.*)`
	receiver := "DUNCP THM NO 074634"
	rePattern := regexp.MustCompile(pattern)
	dataKeys := rePattern.SubexpNames()
	fmt.Println(dataKeys)
	reResult := rePattern.FindAllStringSubmatch(strings.ToUpper(receiver), 1)
	fmt.Println(reResult)

}

func RunMainRegex() {
	var (
		a time.Time = time.Now()
	)
	a.Nanosecond()
	fmt.Println(a.Nanosecond())
	if a.IsZero() {

		fmt.Println(a)
	}
	findPattern()
}

//
type DataValue struct {
	UserName string `json:"username"`
	Password string `json:"password"`
	Test     string `json:"test"`
}

func MaskLogging() string {
	dataSturc := DataValue{
		UserName: "ducnp",
		Password: "123",
		Test:     "3455672134564356",
	}
	byteData, _ := json.Marshal(dataSturc)
	str := MaskJson(string(byteData))
	return str
}

func MaskJson(data string) string {
	jsonMap := make(map[string]interface{})
	if err := json.Unmarshal([]byte(data), &jsonMap); err != nil {
		return data
	}
	fmt.Println("inputData:", data)
	byteData, err := json.Marshal(sentitiveLogging(jsonMap, "test", "(?P<A>[0-9]{3})(?P<SENTITIVE1>[0-9]{3})(?P<SENTITIVE>[0-9]*)(?P<SENTITIVE2>[0-9]{2})(?P<B>[0-9]{2})"))
	if err == nil {
		fmt.Println("out:", string(byteData))
	}
	byteData, err = json.Marshal(sentitiveLogging(jsonMap, "password", "all"))
	if err == nil {
		fmt.Println("out1:", string(byteData))
	}
	return data
}

func sentitiveLogging(data map[string]interface{}, fieldSentitive, fieldMask string) map[string]interface{} {
	if fieldMask == "" || fieldSentitive == "" {
		return data
	}
	for key, value := range data {
		if value == nil || value == "" {
			continue
		}
		switch reflect.TypeOf(value).Kind() {
		case reflect.Slice:
			temporaryMaps := value.([]interface{})
			for _, mapItem := range temporaryMaps {
				if reflect.TypeOf(mapItem).Kind() != reflect.Map {
					continue
				}
				sentitiveLogging(mapItem.(map[string]interface{}), fieldSentitive, fieldMask)
			}
		case reflect.Map:
			sentitiveLogging(value.(map[string]interface{}), fieldSentitive, fieldMask)
		default:
			data[key] = maskLogging(value, key, fieldSentitive, fieldMask)
		}
	}
	return data
}

func maskLogging(value interface{}, key, fieldSentitive, condition string) string {
	defer func() {
		_ = recover()
	}()
	if !strings.EqualFold(key, fieldSentitive) {
		return value.(string)
	}
	conditionStr := strings.ToLower(condition)
	if conditionStr == "" {
		return value.(string)
	}
	strValue := reflect.ValueOf(value).String()
	var maskValue string
	switch conditionStr {
	case "all":
		maskValue = regexp.MustCompile(".").ReplaceAllLiteralString(strValue, "*")
	default:
		re := regexp.MustCompile(condition)
		reValues := re.FindStringSubmatch(strValue)
		reNames := re.SubexpNames()
		for i := 1; i < len(reNames); i++ {
			if len(reValues[i]) > 10 {
				fmt.Println("fsdfds")
			}
			if strings.ToLower(reNames[i]) == "sentitive" {
				maskValue += regexp.MustCompile(".").ReplaceAllLiteralString(reValues[i], "*")
			} else if strings.ToLower(reNames[i]) == "sentitive1" {
				maskValue += regexp.MustCompile(".").ReplaceAllLiteralString(reValues[i], "x")
			} else if strings.ToLower(reNames[i]) == "sentitive2" {
				maskValue += regexp.MustCompile(".").ReplaceAllLiteralString(reValues[i], "z")
			} else {
				maskValue += reValues[i]
			}
		}
	}
	return maskValue

}
