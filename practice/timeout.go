package practice

import (
	"context"
	"fmt"
	"runtime"
	"runtime/debug"
	"strings"
	"sync"
	"time"
)

// https://github.com/zeromicro/go-zero/blob/master/core/fx/timeout.go
// plate visited
// https://github.com/zeromicro/go-zero/blob/master/zrpc/internal/serverinterceptors/timeoutinterceptor.go
func hardWork(job interface{}) error {
	time.Sleep(time.Microsecond * 1000)
	return nil
}
func requestWork(ctx context.Context, job interface{}) error {
	ctx, can := context.WithTimeout(context.Background(), time.Microsecond*500)
	defer can()
	done := make(chan error, 1)
	panicChan := make(chan interface{}, 1)
	go func() {
		defer func() {
			if p := recover(); p != nil {
				panicChan <- fmt.Sprintf("%+v\n\n%s", p, strings.TrimSpace(string(debug.Stack())))
				fmt.Println("oops, panic")
			}
		}()
		done <- hardWork(job)
	}()
	select {
	case err := <-done:
		fmt.Println("done")
		return err
	case p := <-panicChan:
		fmt.Println("pannic ")
		panic(p)
	case <-ctx.Done():
		fmt.Println("ctx done timeout", ctx.Err())
		return ctx.Err()
	}
}

func MainTimeOut() {
	var total = 10
	var wg sync.WaitGroup
	now := time.Now()
	wg.Add(total)
	for i := 0; i < total; i++ {
		go func() {
			defer func() {
				if p := recover(); p != nil {
					fmt.Println("oops, panic")
				}
			}()
			defer wg.Done()

			requestWork(context.Background(), "any")
		}()
	}
	wg.Wait()
	fmt.Println("elapsed:", time.Since(now))
	time.Sleep(time.Second * 2)
	fmt.Println("number of goroutines:", runtime.NumGoroutine())
}
