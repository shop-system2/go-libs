package practice

import (
	"fmt"
)

func printSysbolAndSpace(nSysbol, nSpace int32, isSpace bool) {
	total := nSysbol + nSpace
	var i int32
	for i = 0; i < total; i++ {
		if i < nSpace {
			fmt.Print(".")
		} else {
			fmt.Print("#")
		}

	}
}

func staircase(n int32) {
	// Write your code here
	var i int32
	for i = 1; i <= n; i++ {
		nSysbol := i
		nSpace := n - i
		if nSysbol == n {
			nSpace = 0
		}
		printSysbolAndSpace(nSysbol, nSpace, true)
		fmt.Println()

	}

}

func MainPractice1() {
	// var n int32 = 4
	// staircase(n)
	// var arr = []int32{1, 3, 10, 7, 5, 12, 2, 6, 4}
	var arr = []int64{256741038, 623958417, 467905213, 714532089, 938071625}

	miniMaxSum(arr)

}

// max and min of 4 of 5 element array

func sumSlice(arr []int64) int64 {
	var sum int64
	var i int64
	for i = 0; i < int64(len(arr)); i++ {
		sum += arr[i]
		fmt.Println(arr[i], sum)
	}
	return sum
}

func BubbleSort(arr []int64) {
	var (
		n int64 = int64(len(arr))
		i int64
		j int64
	)
	for i = 0; i < n; i++ {
		for j = 0; j < n-i-1; j++ {
			if arr[j] > arr[j+1] {
				a := arr[j]
				arr[j] = arr[j+1]
				arr[j+1] = a
			}
		}
	}

}

func miniMaxSum(arr []int64) {
	BubbleSort(arr)
	fmt.Println(arr)
	minArr := sumSlice(arr[0 : len(arr)-1])
	maxArr := sumSlice(arr[1:int64(len(arr))])
	fmt.Println(minArr)
	fmt.Println(maxArr)

}
