package list

import "fmt"

// s: s is the number of time, you need to repeat the element
// x: x there are the element in the array
func ListReplication() {
	var s, x int
	list := [4]int{1, 3, 2, 5}
	s = 3
	x = 4
	fmt.Println(s, x, list)
	// step 1: sort array
	for i := 0; i < x; i++ {

	}
}

type listNode struct {
	Val  int
	Next *listNode
}

func AddTwoNumber(l1 *listNode, l2 *listNode) *listNode {
	var (
		// l1       *listNode
		// l2       *listNode
		uintTemp int
		tempVal1 int
		tempVal2 int
		check    = true
	)
	l3 := &listNode{}
	ans := l3
	// l1 = &listNode{
	// 	Val: 9,
	// 	Next: &listNode{
	// 		Val: 9,
	// 		Next: &listNode{
	// 			Val: 9,
	// 			Next: &listNode{
	// 				Val: 9,
	// 				Next: &listNode{
	// 					Val: 9,
	// 					Next: &listNode{
	// 						Val: 9,
	// 						Next: &listNode{
	// 							Val:  9,
	// 							Next: nil,
	// 						},
	// 					},
	// 				},
	// 			},
	// 		},
	// 	},
	// }

	// l2 = &listNode{
	// 	Val: 9,
	// 	Next: &listNode{
	// 		Val: 9,
	// 		Next: &listNode{
	// 			Val: 9,
	// 			Next: &listNode{
	// 				Val:  9,
	// 				Next: nil,
	// 			},
	// 		},
	// 	},
	// }

	for {
		// work
		if l1 == nil && l2 == nil && uintTemp == 0 {
			break
		}
		if l1 == nil {
			tempVal1 = 0
		} else {
			tempVal1 = l1.Val
			l1 = l1.Next
		}

		if l2 == nil {
			tempVal2 = 0
		} else {
			tempVal2 = l2.Val
			l2 = l2.Next
		}
		temSum := tempVal1 + tempVal2 + uintTemp
		uintTemp = temSum / 10
		temSum %= 10

		if check {
			l3.Val = temSum
			check = false
		} else {
			node := &listNode{
				Val:  temSum,
				Next: nil,
			}
			l3.Next = node
			l3 = l3.Next
		}
	}
	return ans
}
