package list

import (
	"fmt"
	"math"
	"sort"
)

// check isPrime and isPalindrome

func IsPrime(number int) bool {
	if number == 1 {
		return true
	}
	fmt.Println(math.Sqrt(float64(number)))
	for i := 2; i < int(math.Sqrt(float64(number))); i++ {
		if number%i == 0 {
			return false
		}
	}
	return true
}

func IsPalindrome(number int) bool {
	// if !IsPrime(number) {
	// 	return false
	// }
	if number == 1 {
		return true
	}
	if number < 0 {
		return false
	}
	oldNumber := number
	var numberCheck int
	for {
		a := number / 10
		b := number % 10
		numberCheck = numberCheck*10 + b
		number = a
		if number == 0 {
			break
		}
	}
	return oldNumber == numberCheck
}

func nSum(nums []int, n int, start int, target int) [][]int {
	var result [][]int
	fmt.Println(nums)
	if n < 2 || len(nums) < n {
		return result
	}
	if n == 2 {
		left, right := start, len(nums)-1
		for left < right {
			low := nums[left]
			high := nums[right]
			sum := low + high
			if sum < target {
				for left < right && nums[left] == low {
					left++
				}
			} else if sum > target {
				for left < right && nums[right] == high {
					right--
				}
			} else if sum == target {
				result = append(result, []int{low, high})
				for left < right && nums[left] == low {
					left++
				}
				for left < right && nums[right] == high {
					right--
				}
			}
		}
	} else {
		for i := start; i < len(nums); i++ {
			newTarget := target - nums[i]
			sub := nSum(nums, n-1, i+1, newTarget)
			fmt.Println(sub)
			for _, v := range sub {
				v = append(v, nums[i])
				result = append(result, v)
			}
			for i < len(nums)-1 && nums[i] == nums[i+1] {
				i++
			}
		}
	}
	return result
}

//
func FourSum(nums []int, target int) [][]int {
	rs := make([][]int, 0)
	//special
	if len(nums) < 4 {
		return rs
	}
	//sort
	sort.Ints(nums)
	//main
	for i := 0; i < len(nums)-3; i++ {
		//special
		// recheckIncre := nums[i] + nums[i+1] + nums[i+2] + nums[i+3]
		// if recheckIncre > target {
		// 	break
		// }
		// recheckDescre := nums[i] + nums[len(nums)-3] + nums[len(nums)-2] + nums[len(nums)-1]
		// if recheckDescre < target {
		// 	continue
		// }
		//main
		for j := i + 1; j < len(nums); j++ {
			k, h := j+1, len(nums)-1
			for k < h {
				curSum := nums[i] + nums[j] + nums[k] + nums[h]
				if curSum == target {
					rs = append(rs, []int{nums[i], nums[j], nums[k], nums[h]})
					k++
					h--
					for k < h && nums[k] == nums[k-1] {
						k++
					}
					for k < h && nums[h] == nums[h+1] {
						h--
					}
				} else if curSum > target {
					h--
				} else {
					k++
				}
			}
			for j < len(nums)-2 && nums[j+1] == nums[j] {
				j++
			}
		}
		for i < len(nums)-1 && nums[i+1] == nums[i] {
			i++
		}
	}
	return rs
}

func GetIndexNandValue(nums []int, possition int) (valueOut, indexOut int) {
	newArrr := []int{}
	tempMap := make(map[int]int)
	mapIndex := make(map[int]int)
	for i := 0; i < len(nums); i++ {
		mapIndex[nums[i]] = i
	}

	for i := 0; i < len(nums); i++ {
		for j := i + 1; j < len(nums); j++ {
			if nums[j] > nums[i] {
				temp := nums[i]
				nums[i] = nums[j]
				nums[j] = temp
			}
		}
	}

	for i := 0; i < len(nums); i++ {
		if _, ok := tempMap[nums[i]]; ok {
			continue
		}
		tempMap[nums[i]] = i
		newArrr = append(newArrr, nums[i])
	}

	valueOut = newArrr[possition-1]
	if value, ok := mapIndex[valueOut]; ok {
		indexOut = value
	}
	fmt.Println(valueOut, indexOut)
	return valueOut, indexOut
}

func IsPowerOfTwo(n int) bool {
	if n == 0 {
		return false
	}
	newn := float64(n)
	return (math.Ceil(math.Log2(newn)) == math.Floor((math.Log2(newn))))
}

func FindDisappearedNumbers(nums []int) []int {
	var (
		result  []int
		maptemp map[int]int = make(map[int]int)
	)
	max := 0

	for i := 0; i < len(nums); i++ {
		if nums[i] > max {
			max = nums[i]
		}
		maptemp[nums[i]] = i
	}

	for i := 1; i <= len(nums); i++ {
		_, ok := maptemp[i]
		if !ok {
			result = append(result, i)
		}
		// if value; ok := maptemp[i] ; !ok {
		// 	result = append(result, i)
		// }
	}
	fmt.Println(result)
	return result
}
