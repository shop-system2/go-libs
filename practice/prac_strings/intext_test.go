package prac_strings

import (
	"reflect"
	"testing"
)

var TestCase = []struct {
	name     string
	input    string
	pattern  string
	expected []int
}{
	{
		"string with multiple pattern matches",
		"ABAAABCDBBABCDDEBCABC",
		"ABC",
		[]int{4, 10, 18},
	},
}

func TestNative(t *testing.T) {
	for _, tc := range TestCase {
		t.Run(tc.name, func(t *testing.T) {
			actual := Native(tc.input, tc.pattern)
			if !reflect.DeepEqual(actual, tc.expected) {
				t.Errorf("Expected matches for pattern '%s' for string '%s' are: %v, but actual matches are: %v", tc.pattern, tc.input, tc.expected, actual)
			}
		})
	}
}

// go test
