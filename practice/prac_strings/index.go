package prac_strings

import "fmt"

func Native(text string, pattern string) []int {
	var position []int
	sub := len(text) - len(pattern)
	if sub < 0 {
		return position
	}
	var match bool
	for i := 0; i <= sub; i++ {
		for j := 0; j < len(pattern); j++ {
			match = true
			if text[i+j] != pattern[j] {
				match = false
				break
			}
		}
		if match {
			position = append(position, i)
		}
	}

	return position
}

func SearchBoyerMoore(text string, pattern string) []int {
	var position []int

	l := len(text)
	n := len(pattern)

	sub := l - n
	if sub < 0 {
		return position
	}

	bcr := make(map[byte]int)
	for i := 0; i < n-1; i++ {
		bcr[pattern[i]] = n - i - 1
	}

	return position
}

func MainString() {
	text := "aa123a123m123m123m"
	pattern := "123"
	poss := Native(text, pattern)
	fmt.Println(poss)
}
