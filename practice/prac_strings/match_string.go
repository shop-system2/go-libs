package prac_strings

import "fmt"

func find(strings []string, itemInput string) int32 {
	var count int32 = 0
	for _, item := range strings {
		if item == itemInput {
			count++
		}
	}
	return count
}

func matchingStrings(strings []string, queries []string) []int32 {
	// Write your code here
	result := make([]int32, len(queries))
	for i, item := range queries {
		temp := find(strings, item)
		result[i] = temp
	}
	return result
}

func MainmatchingStrings() {
	// queries := []string{"aba", "xzxb", "ab"}
	// strings := []string{"aba", "baba", "aba", "xzxb"}

	queries := []string{"de", "lmn", "fgh"}
	strings := []string{"def", "de", "fgh"}

	retur := matchingStrings(strings, queries)
	fmt.Println(retur)
}
