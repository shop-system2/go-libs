package goroutine

import (
	"context"
	"fmt"
	"sort"
	"sync"
	"time"

	"golang.org/x/time/rate"
)

// type APIConn struct {
// 	rateLimiter *rate.Limiter
// }
type APIConn struct {
	// rateLimiter *rate.Limiter
	apiLimit,
	dbLimit RateLimiter
}
type RateLimiter interface {
	Wait(context.Context) error
	Limit() rate.Limit
}

type multiLimiter struct {
	limiters []RateLimiter
}

func MultiLimiter(limiters ...RateLimiter) *multiLimiter {
	byLimit := func(i, j int) bool {
		return limiters[i].Limit() < limiters[j].Limit()
	}

	sort.Slice(limiters, byLimit)
	return &multiLimiter{limiters: limiters}
}
func OpenApi() *APIConn {
	return &APIConn{
		apiLimit: MultiLimiter(
			rate.NewLimiter(Per(2, time.Second), 5),
			rate.NewLimiter(Per(5, time.Minute), 5),
		),
		dbLimit: MultiLimiter(
			rate.NewLimiter(rate.Every(time.Second*5), 1),
		),
		// rateLimiter: rate.NewLimiter(rate.Every(time.Second), 1),
	}
}
func (l *multiLimiter) Wait(ctx context.Context) error {
	for _, l := range l.limiters {
		if err := l.Wait(ctx); err != nil {
			return err
		}
	}

	return nil
}

func (l *multiLimiter) Limit() rate.Limit {
	return l.limiters[0].Limit()
}

func Per(eventCount int, duration time.Duration) rate.Limit {
	return rate.Every(duration / time.Duration(eventCount))
}

func (c *APIConn) Read(ctx context.Context) (string, error) {
	err := c.apiLimit.Wait(ctx)
	if err != nil {
		return "", err
	}

	// do work
	return "Read", nil
}

func (c *APIConn) Resolve(ctx context.Context) error {
	err := c.apiLimit.Wait(ctx)
	if err != nil {
		return err
	}
	// do work
	return nil
}

func RunMainRateLimiting() {
	fmt.Println("application running")
	defer fmt.Println("exiting application")
	apiConnection := OpenApi()
	var wg sync.WaitGroup
	wg.Add(20)
	for i := 0; i < 10; i++ {
		go func() {
			defer wg.Done()

			v, err := apiConnection.Read(context.Background())
			if err != nil {
				fmt.Printf("%v Get Error: %v\n", time.Now().Format("15:04:05"), err)
				return
			}

			fmt.Printf("%v %v\n", time.Now().Format("15:04:05"), v)

		}()
	}

	for i := 0; i < 10; i++ {
		go func() {
			defer wg.Done()

			err := apiConnection.Resolve(context.Background())
			if err != nil {
				fmt.Printf("%v Resolve Error: %v\n", time.Now().Format("15:04:05"), err)
				return
			}

			fmt.Printf("%v Resolved\n", time.Now().Format("15:04:05"))
		}()
	}

	wg.Wait()
}
