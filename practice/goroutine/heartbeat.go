package goroutine

import (
	"fmt"
	"time"
)

func RunMainGoroutine() {
	done := make(chan interface{})
	time.AfterFunc(10*time.Second, func() {
		close(done)
	})

	const timeout = 2 * time.Second
	heartbeat, results := doSomething(done, timeout/2)

	for {
		select {
		case _, ok := <-heartbeat:
			if ok == false {
				fmt.Println("heartbeat false")
				return
			}
			fmt.Println("heartbeat")
		case r, ok := <-results:
			if ok == false {
				fmt.Println("result false")
				return
			}
			fmt.Printf("doSomething result %v\n", r)
		case <-time.After(timeout):
			fmt.Println("timeout")
			return
		}
	}

}

func doSomething(done <-chan interface{}, pulseInterval time.Duration) (<-chan interface{}, <-chan time.Time) {
	heartbeat := make(chan interface{})
	results := make(chan time.Time)
	go func() {
		defer close(heartbeat)
		defer close(results)

		pulse := time.Tick(pulseInterval)
		doWork := time.Tick(2 * pulseInterval)
		fmt.Println("tick pulse")

		sendPulse := func() {
			select {
			case heartbeat <- struct{}{}:
				fmt.Println("case send pulse")
			default:
				fmt.Println("defalut send pulse")
			}
		}

		sendResult := func(r time.Time) {
			for {
				select {
				case <-done:
					fmt.Println("11")

					return
				case <-pulse:
					fmt.Println("222")

					sendPulse()
				case results <- r:
					fmt.Println("33")

					return
				}
			}
		}

		for {
			select {
			case <-done:
				fmt.Println("44")
				return
			case <-pulse:
				sendPulse()
				// fmt.Println("pulse")
			case r := <-doWork:
				fmt.Println("send:", r)
				sendResult(r)
			}
		}

	}()

	return heartbeat, results
}
