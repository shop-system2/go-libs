package goroutine

import (
	"fmt"
	"time"
)

func MainChannel() {

	fmt.Println("MainChannel")
	cha := make(chan string)
	// cha1 := make(chan string)
	go test(cha)
	go test1(cha)
	go test1(cha)

	chValue := <-cha
	// chValue1 := <-cha1
	fmt.Println(chValue)
	chValue1 := <-cha

	fmt.Println(chValue1)
}

func test(s chan string) {
	time.Sleep(time.Second)
	s <- "test ok"
}
func test1(s chan string) {
	time.Sleep(time.Second * 3)
	s <- "test1 ok"
}
