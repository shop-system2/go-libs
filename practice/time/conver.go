package time

import (
	"fmt"
	"strconv"
	"strings"
	"time"
)

func RunMainTime() {
	// timeSub()
	ConverUTCTotime(1650452426)
	// -7 : 1650432746  1650427226
	// 0: 1650457946
}

// convert time +07 to utc
func convert07ToUTC(in time.Time) time.Time {
	name, _ := in.Zone()
	if name == "UTC" {
		return in
	}
	return in.Add(-time.Hour * 7)
}

func ConverUTCTotime(in int64) string {

	unixTimeUTC := time.Unix(in, 7) //gives unix time stamp in utc
	// unixTimeUTC = convert07ToUTC(unixTimeUTC)
	unitTimeInRFC3339 := unixTimeUTC.Format("2006-01-02 15:04:05") // converts utc time to RFC3339 format
	return unitTimeInRFC3339
}

func ConvertTimeYYYYMMDDToUnix(date string) int64 {
	t, err := time.Parse("2006-01-02 15:04:05", date)
	if err != nil {
		return 0
	}
	return convert07ToUTC(t).Unix()
}

func TimeConversion(s string) string {
	var outTime string
	if strings.HasSuffix(s, "PM") {
		timeSplit := strings.Split(s, "PM")[0]
		subTime := strings.Split(timeSplit, ":")[0]
		if subTime == "12" {
			outTime = timeSplit
		} else {
			parserTime, _ := time.Parse("15:04:05", timeSplit)
			newTime := parserTime.Add(time.Hour * 12)
			outTime = newTime.Format("15:04:05")
		}

	} else {
		outTime = strings.Split(s, "AM")[0]
		subTime := strings.Split(outTime, ":")[0]
		if subTime == "12" {
			parserTime, _ := time.Parse("15:04:05", outTime)
			newTime := parserTime.Add(time.Duration(-12) * time.Hour)
			outTime = newTime.Format("15:04:05")
		}
	}
	return outTime
}
func FormatBirthDateIntToString() {

	timeNew, err := time.Parse("2006-01-02 15:04:05", "732301200")
	fmt.Println(err)
	fmt.Println(timeNew)

	i, err := strconv.ParseInt("732301200", 10, 64)
	if err != nil {
		panic(err)
	}
	tm := time.Unix(i, 0).Format("2006-01-02")
	fmt.Println(tm)
}

func BeforeAfterNumberDay(number int) (string, string) {
	timeNow := time.Now()
	if number == 0 {
		return "", ""
	}
	fotmatLayout := "2006-01-02"
	timeStartDay := "00:00:00.0"
	timeEndDay := "23:59:59.59"
	if number == 1 {
		return timeNow.Format(fotmatLayout) + timeStartDay, timeNow.Format(fotmatLayout) + timeEndDay
	}
	addHour := (number - 1) * 24
	previousDay := timeNow.Add(time.Duration(-addHour) * time.Hour).Format(fotmatLayout)
	nextDay := timeNow.Add(time.Duration(addHour) * time.Hour).Format(fotmatLayout)
	return previousDay + timeStartDay, nextDay + timeEndDay

}

func timeSub() {
	fmt.Println("year:", time.Now().Year())
	startTime := time.Now().AddDate(0, 0, -2)
	fmt.Println(startTime.Format("2006-01-02 15:04:05"))
	entime := time.Now().AddDate(0, 0, +2)
	fmt.Println(entime.Format("2006-01-02 15:04:05"))

	timeNow := time.Now()
	fmt.Println(timeNow.Format("2006-01-02 15:04:05"))

	startsub := timeNow.Sub(startTime).Seconds() // > 0
	fmt.Println(startsub)
	endsub := timeNow.Sub(entime).Seconds() // < 0
	fmt.Println(endsub)

	fmt.Println(int(entime.Day()))
	fmt.Println(int(timeNow.Day()))
	fmt.Println((startTime.Day()))

	fmt.Println(int(entime.Month()))
	fmt.Println(int(startTime.Month()))

	if int(timeNow.Day()) >= (startTime.Day()) && int(timeNow.Day()) <= (entime.Day()) {
		fmt.Println("ok")
	}

}
