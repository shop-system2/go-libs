package image

import (
	"encoding/base64"
	"fmt"
	"net/http"
	"os"
	"strings"
	"unsafe"
)

func ConvertBase64ToImage(base64I *string) {
	fmt.Printf("a: %T, %d\n", base64I, unsafe.Sizeof(base64I))

	if base64I == nil {
		panic("nill")
	}
	intBa := len(*base64I)
	lenBa := base64.StdEncoding.DecodedLen(intBa)
	fmt.Println(lenBa)
	byteImage, err := base64.StdEncoding.DecodeString(*base64I)
	fmt.Println(err)
	sdf := intBa * intBa / lenBa
	fmt.Println(sdf)

	// byteReader := bytes.NewReader(byteImage)

	// fmt.Println(byteReader, err)
	mimeType := http.DetectContentType(byteImage)
	var base64Encoding string

	if !strings.Contains(mimeType, "jpeg") || !strings.Contains(mimeType, "png") {
		fmt.Println("sdfsdf")
	}

	// Prepend the appropriate URI scheme header depending
	// on the MIME type
	switch mimeType {
	case "image/jpeg":
		base64Encoding += "data:image/jpeg;base64,"
	case "image/png":
		base64Encoding += "data:image/png;base64,"
	}

	f, err := os.Create("myfilename.png")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	if _, err := f.Write(byteImage); err != nil {
		panic(err)
	}
	if err := f.Sync(); err != nil {
		panic(err)
	}
	fmt.Println("Ok")
}
