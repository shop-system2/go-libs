package practice

import "fmt"

func rotate(maxtrix [][]int) {
	fmt.Println("rotate")
	resultMatrix := make([][]int, len(maxtrix))
	fmt.Println(resultMatrix)
	for i := 0; i < len(maxtrix); i++ {
		for j := 0; j < len(maxtrix[i]); j++ {
			fmt.Println(j, i, maxtrix[i][j])
		}
	}
}

func RunRotate() {
	matrix := [][]int{
		{1, 2},
		{3, 4},
	}
	// => [[3,1],[4,2]]
	//  => 2,0 -> 0,0 = len(i)-1-i=2, j=0
	// 1,0 -> 0,1 ;
	// 0,0 -> 0,2;

	// 2,1 -> 1,0
	// 1,1 -> 1,1
	// 0,1 -> 1,2

	rotate(matrix)
}
