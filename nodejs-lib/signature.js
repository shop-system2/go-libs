

// var CryptoJS = require("crypto-js");

// const SHA256Salt = 'GO@2021';

// const sha256 = text => {
//   text = text + SHA256Salt
  
//   const hmacSha256 = CryptoJS.HmacSHA256(text).toString()
//   console.log("hmacSha256:",hmacSha256)

//   return CryptoJS.SHA256(text).toString();
// };

// const flattenObj = (ob) => {
//   let result = {};
//   for (const i in ob) {
//     if ((typeof ob[i]) === 'object') {
//       const temp = flattenObj(ob[i]);
//       for (const j in temp) {
//         result[i + '.' + j] = temp[j];
//       }
//     }
//     else {
//       result[i] = ob[i];
//     }
//   }
//   return result;
// };

// const sha256SignObject = rawObject => {
//   console.log("rawObject:",rawObject)

//   const obj = flattenObj(rawObject);
//   console.log("obj:",obj)
//   const keys = Object.keys(obj).filter(key => key !== 'signature');
//   // console.log("keys:",keys)
//   keys.sort();
//   // console.log("keys:",keys)

//   let concatValue = '';
//   for (let key of keys) concatValue += obj[key];
//   console.log("concatValue:",concatValue)

//   return sha256(concatValue);
// };

// const p = {
//     "orderId": "f5b2d33d-2fb3-4c6a-b0f7-b68111944adb",
//     "responseCode": "00",
//     "responseMessage": "SUCCESS"
// }

// p.signature = sha256SignObject(p);
// console.log("signature:",sha256SignObject(p))

// 7c5c8805ef040bb6b3d2db34d6ac8c0c23409a404d04ea21b16718805db2478a  -> nodejs
// c1e0072742a3176ed5085e990d4e2d3ce06f01bfb2ce56c7f247f4384818dfca   -> tool online
// c1e0072742a3176ed5085e990d4e2d3ce06f01bfb2ce56c7f247f4384818dfca  -> golang

var CryptoJS = require("crypto-js");

const SHA256Salt = 'GO@2021';

const sha256 = text => {
  console.log("plainTxt:",text + SHA256Salt)

  return CryptoJS.SHA256(text + SHA256Salt).toString();
};

const flattenObj = (ob) => {
  let result = {};
  for (const i in ob) {
    if ((typeof ob[i]) === 'object') {
      const temp = flattenObj(ob[i]);
      for (const j in temp) {
        result[i + '.' + j] = temp[j];
      }
    }
    else {
      result[i] = ob[i];
    }
  }
  return result;
};

const sha256SignObject = rawObject => {
  const obj = flattenObj(rawObject);
  const keys = Object.keys(obj).filter(key => key !== 'signature');
  keys.sort();
  let concatValue = '';
  for (let key of keys) concatValue += obj[key];

 
  return sha256(concatValue);
};

const p = {
    "orderId": "aee40925-1ddc-4ab8-afec-03ebb4ffc018",
    "responseCode": "200",
    "responseMessage": "SUCCESS",
    "payAmount": 230000
}

p.signature = sha256SignObject(p);
console.log(p.signature)
// pm.environment.set('payload', JSON.stringify(p))
// af794989585578c226648ebe34504b97e6ecb38c148d8a556a6e97de2ac0db1e
// af794989585578c226648ebe34504b97e6ecb38c148d8a556a6e97de2ac0db1e
// af794989585578c226648ebe34504b97e6ecb38c148d8a556a6e97de2ac0db1e
// plainTxt: aee40925-1ddc-4ab8-afec-03ebb4ffc018230000200SUCCESSGO@2021 -> signature: 8e713d57f970274852c924d9a71cbcbf4506e866a0903df5efd6bc3f6c1a5314

// OrderId:"aee40925-1ddc-4ab8-afec-03ebb4ffc018", ResponseCode:"200", ResponseMessage:"SUCCESS", Amount:230000, Signature:"8e713d57f970274852c924d9a71cbcbf4506e866a0903df5efd6bc3f6c1a5314"}, Method:"", Url:"http://14.161.20.182:6969/v2/ipn-qr", Header:map[string]string{"Content-Type":"application/json"}}
