FROM ubuntu:latest
ARG name=ducnp
RUN echo "Hey there! Welcome to $name" > greeting.txt
CMD cat greeting.txt

#  sudo docker build -t arg-demo . -f arg.Dockerfile
# sudo docker run -it arg-demo bash

# you can override the default value of ARG  by using the -build-arg 
# sudo docker build -t arg-demo --build-arg GREET=World .