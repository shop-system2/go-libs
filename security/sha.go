package security

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
)

const (
	secretKey string = "hdbankitd"
)

type Profile struct {
	AcctNo              string
	BankTransactionId   string
	TransactionAmount   string
	TransactionDateTime string
	Narrative           string
}

func salt(in Profile) (string, error) {
	data := in.AcctNo + in.BankTransactionId + in.TransactionAmount + in.TransactionDateTime + in.Narrative
	strSecret := data + secretKey
	return strSecret, nil
}

// sha256
func MainSha256() string {
	pro := Profile{
		AcctNo:              "2738939393",
		BankTransactionId:   "1231294239048",
		TransactionAmount:   "200100",
		TransactionDateTime: "2022-08-24 00-00-00",
		Narrative:           "fsdf",
	}
	strSalt, err := salt(pro)
	if err != nil {
		panic(err)
	}
	hash256 := sha256.New()
	_, err1 := hash256.Write([]byte(strSalt))
	if err1 != nil {
		panic(err1)
	}
	signatureByte := hash256.Sum([]byte(secretKey))
	signature := fmt.Sprintf("%x", signatureByte)
	fmt.Println("sha256 signature:", signature)
	return signature
}

func MainHmac() string {
	pro := Profile{
		AcctNo:              "2738939393",
		BankTransactionId:   "1231294239048",
		TransactionAmount:   "200100",
		TransactionDateTime: "01-MAR-22",
		Narrative:           "fsdf",
	}
	data := pro.AcctNo + pro.BankTransactionId + pro.TransactionAmount + pro.TransactionDateTime + pro.Narrative
	fmt.Println(data)
	fmt.Println("010704070030659101613129039012831238904000001-MAR-22devsstest")
	mac := hmac.New(sha256.New, []byte(secretKey))
	mac.Write([]byte(data))
	expectedMAC := mac.Sum(nil)

	sign := hex.EncodeToString(expectedMAC)

	fmt.Println("hmac signature: ", sign)

	a := string(expectedMAC)
	fmt.Println(a)

	messageMAC := "303333353237393138339fe89654590cd75604cede3eef2c19d1fe2aa28093188285940f6c97fd3f0bd4"
	equam := hmac.Equal([]byte(messageMAC), expectedMAC)
	fmt.Println(equam)

	return ""
}

func hamc() {
	secret := "mysecret"
	data := "data"
	fmt.Printf("Secret: %s Data: %s\n", secret, data)

	// Create a new HMAC by defining the hash type and the key (as byte array)
	h := hmac.New(sha256.New, []byte(secret))

	// Write Data to it
	h.Write([]byte(data))

	// Get result and encode as hexadecimal string
	// sha := hex.EncodeToString(h.Sum(nil))
	signatureByte := h.Sum([]byte(secretKey))
	signature := hex.EncodeToString(signatureByte)
	// signature := fmt.Sprintf("%x", signatureByte)
	fmt.Println("Result: " + signature)
}

func MainSecurity() {
	// MainSha256()
	// hamc()
	MainHmac()
}
