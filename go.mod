module go-libs

go 1.16

require (
	github.com/DataDog/zstd v1.5.2 // indirect
	github.com/RediSearch/redisearch-go v1.1.1
	github.com/Shopify/toxiproxy v2.1.4+incompatible // indirect
	github.com/aws/aws-lambda-go v1.34.1
	github.com/aws/aws-sdk-go v1.44.76
	github.com/cheekybits/genny v1.0.0
	github.com/eapache/go-resiliency v1.3.0 // indirect
	github.com/eapache/go-xerial-snappy v0.0.0-20180814174437-776d5712da21 // indirect
	github.com/eapache/queue v1.1.0 // indirect
	github.com/frankban/quicktest v1.14.3 // indirect
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/go-redis/redis/v8 v8.11.5
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gogo/protobuf v1.3.2
	github.com/golang/protobuf v1.5.2
	github.com/gomodule/redigo v1.8.8 // indirect
	github.com/google/uuid v1.1.2
	github.com/gorilla/websocket v1.4.2
	github.com/grpc-ecosystem/go-grpc-middleware v1.3.0
	github.com/jinzhu/copier v0.2.8
	github.com/mediocregopher/radix/v3 v3.8.0
	github.com/nitishm/go-rejson/v4 v4.1.0
	github.com/pierrec/lz4 v2.6.1+incompatible // indirect
	github.com/rcrowley/go-metrics v0.0.0-20201227073835-cf1acfcdf475 // indirect
	github.com/segmentio/kafka-go v0.4.34
	github.com/sijms/go-ora/v2 v2.5.3
	github.com/spf13/cobra v1.3.0
	go.uber.org/zap v1.21.0
	golang.org/x/net v0.0.0-20220706163947-c90051bbdb60
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
	golang.org/x/time v0.0.0-20210220033141-f8bda1e9f3ba
	google.golang.org/grpc v1.42.0
	gopkg.in/Shopify/sarama.v1 v1.20.1
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
	gopkg.in/yaml.v3 v3.0.1
)
