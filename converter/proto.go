package converter

import (
	"github.com/gogo/protobuf/proto"
	"github.com/jinzhu/copier"
)

type (
	PbCopier interface {
		CopyToPb(protoc proto.Message, from interface{})
		CopyFromPb(to interface{}, protoc proto.Message)
		CopySliceToSlice() error
		CopyMapToMap(toMap interface{}, fromMap interface{}) error
	}

	ModelCopier interface {
		CopyFromModel(to interface{}, from interface{})
		CopyToModel(to interface{}, from interface{})
	}

	pbCopier struct {
	}

	modelCopier struct {
	}
)

func NewPbCopier() PbCopier {
	return &pbCopier{}
}

func (s *pbCopier) CopyMapToMap(tomap interface{}, frommap interface{}) error {
	err := copier.Copy(tomap, frommap)
	if err != nil {
		return err
	}
	return nil
}

func (s *pbCopier) CopySliceToSlice() error {
	return nil
}

func (s *pbCopier) CopyToPb(to proto.Message, from interface{}) {
	err := copier.Copy(to, from)
	if err != nil {
		return
	}
}

func (s *pbCopier) CopyFromPb(to interface{}, from proto.Message) {
	_ = copier.Copy(to, from)
}

func NewModelCopier() ModelCopier {
	return &modelCopier{}
}

func (s *modelCopier) CopyToModel(to, from interface{}) {
	_ = copier.Copy(to, from)
}

func (s *modelCopier) CopyFromModel(to, from interface{}) {
	_ = copier.Copy(to, from)
}
