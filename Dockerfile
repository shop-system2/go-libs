FROM golang:1.15 as builder
WORKDIR /app
COPY . /app
RUN go build -o runapp main.go

FROM golang:1.15
WORKDIR /app
COPY --from=builder /app/runapp app/library.go
CMD [".library.go"]